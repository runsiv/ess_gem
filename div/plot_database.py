import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import sys
import glob
import pathlib
import matplotlib.pyplot as plt
import sqlalchemy
from sqlalchemy import create_engine


def connect_to_db():
    #print(self.db)
    try:
        conn = create_engine('2019-00105.db')
        cur = conn.connect()

    except Error as e:
        print(e)
    return conn,cur
    #print(sn)

def db_to_df(conn):
    df_ut = pd.read_sql_table('Ultrasound', con = conn)
    df_t = pd.read_sql_table('Temperature', con = conn)
    return df_ut, df_t

if __name__ == "__main__":
    conn, cur = connect_to_db()
    df_ut ,df_t = db_to_df(cur)
    plt.figure()
    df_t.plot()