import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
import os 
import sys 
from tkinter import filedialog
from tkinter import *
import numpy as np
root = Tk()
root.withdraw()
folder_selected = filedialog.askdirectory()


os.chdir(folder_selected)


style.use('fivethirtyeight')

fig = plt.figure()
ax1 = fig.add_subplot(1,1,1)


#temperature_probe.csv

def animate(i):
    with open('temperature_probe.csv') as f:
        graph_data = f.read()

    #graph_data = open('temperature_probe.csv','r').read()
    lines = graph_data.split('\n')
    
    xs = []
    yint= []
    ys = []
    ys1 = []
    ys2 = []
    ys3 = []
    ys4 = []
    ys5 = []
    ys6 = []
    for line in lines:
        if len(line) > 1:
            x, yi, y, y2, y3, y4, y5, y6, y7 = line.split(';')
            xs.append((x))
            yint.append(float(yi)) 
            ys.append(float(y))
            ys1.append(float(y2))
            ys2.append(float(y3))
            ys3.append(float(y4))
            ys4.append(float(y5))
            ys5.append(float(y6))
            ys6.append(float(y7))
   
 
           
    
    ax1.clear()
    ax1.plot(xs, yi, label='a')
    ax1.plot(xs, ys, label='Sensor 1')
    ax1.plot(xs, ys1, label='Sensor 2')
    ax1.plot(xs, ys2, label='Sensor 3')
    ax1.plot(xs, ys3, label='Sensor 4')
    ax1.plot(xs, ys4, label='Sensor 5')
    ax1.plot(xs, ys5, label='Sensor 6')
    ax1.plot(xs, ys6, label='Sensor 7')
    
    #ax1.xaxis.set_major_locator(plt.MaxNLocator(40))


    
    #ax1.xticks([])
    #fig.xticks([])
    plt.xticks([])
    #plt.MaxNLocator(20)
    plt.legend(loc="lower right")
    plt.xticks(rotation='vertical')
    #plt.locator_params(axis='xs', nbins=20)



ani = animation.FuncAnimation(fig, animate, interval=30000)
plt.show()