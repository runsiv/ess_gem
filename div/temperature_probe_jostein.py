

import time
import os
import glob
import sys
import logging
import traceback
import argparse

import datetime
from datetime import date
import csv
import serial
import um_functions.um_func as um_func
import numpy as np
#import um_functions.wall_thickness as wt

parser = argparse.ArgumentParser(description="Temp logger script")
parser.add_argument('sdb1', help='Com port for SDB1')
parser.add_argument('address', help='"S2M64 Address')
parser.add_argument('NumberOfSensors', help= "Number of temp sensors")
#parser.add_argument('sdb2', help='Com port for SDB2')
parser.add_argument('interval', help='Samling interval in minutes')
#parser.add_argument('mode', help=" 0 for test blocks, 1 for +-2 degree with external ref, 2 for +-5 with external ref, 3 for +-2 with user input start temp, 4 for +-5 with user input start temp  ")


args = parser.parse_args()


um = um_func.SwarmActions()
path = os.getcwd()
test_folder = '\\test_data'
new_path = path+test_folder
if not os.path.isdir(new_path):
    os.makedirs(new_path)

os.chdir(new_path)
logging.basicConfig( filename='temperature_probe.txt', level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )
#logging.basicConfig( level=logging.WARNING, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )

""" logging.basicConfig()
logger = logging.getLogger('datalogger')
logger.setLevel(logging.INFO) """
#######################
str_start = 'Transducer Holder Q2 test starting'
print(str_start)
logging.info(str_start)


ser_port = args.sdb1
sdb1 = serial.Serial(ser_port)  # open serial port sdb1
sdb1.baudrate = 115200
sdb1.timeout = 1
sdb1.xonxoff = False

S2M64_add = int(args.address)
numOfSensors = int(args.NumberOfSensors) + 1

meas_interval = int(args.interval)

# measures internal temperature and US on S2M64
def measurement(sbd1, S2M64_add):
    
    status = "Starting measureing from S2M64 with address: %d" % (
        S2M64_add)
    print(status)
    dt = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S ")

    # Read Serial Number of S2M64
    try:
        SN = um.read_serial(sdb1, S2M64_add)
        SN = SN.decode("ascii")
    except:
        SN = "SN error"

    #print("Starting reading temperature")
    temp_list = []
    temp_list.append(dt)
    for i in range(0, numOfSensors):

        try:
            t = um.measure_temperature_ext(sdb1, S2M64_add, i)
            temp_list.append(t)
            #diff_last_meas = t-t_old[i-1]
            #diff_start = t_start[i]
            #printstring = "Temp: {} diff last meas: {} ".format(t, diff_last_meas)
            printstring = "Temp {}: {} ".format(i,t)
            print(printstring)
        except:
            print("Error reading temperature sensor {}".format(i))
            t= -5000
            temp_list.append(t)
    with open ("temperature_probe.csv", 'a') as f:
        #S2 = ([dt, SN, temp_list[0], temp_list[1], temp_list[2], temp_list[3]])# temp_list[4],temp_list[5],temp_list[6], temp_list[7], temp_list[8], temp_list[9] ])
        wr=csv.writer(f, dialect= 'excel', delimiter=';', lineterminator='\n')
        wr.writerow(temp_list)
    status ="Done with S2M64 with address %d "% (S2M64_add)
    print(status)
    #temp_list_old = temp_list
    logging.info(status +dt)
    dt = datetime.datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S ")
    print("Done, last measurement time:" + dt)
    #return temp_list_old

# log once every n minut
"""
t_old = []
for i in range(numOfSensors):
    t_old.append(0)
"""
#t_start = t_old
#t_start = measurement(sdb1,S2M64_add,t_old,t_start)
measurement(sdb1,S2M64_add)
while(1):
    start = datetime.datetime.now()
    next_interval = start + datetime.timedelta(minutes=meas_interval)
    print("Next measurement", next_interval)
    while next_interval  > datetime.datetime.now():
        time.sleep(1)
    measurement(sdb1,S2M64_add)
exit()
