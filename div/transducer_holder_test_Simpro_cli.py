import serial
import time
import numpy as np
import um_functions.um_func as um_func
import argparse
parser = argparse.ArgumentParser(description="Script for measureing either ultrasound or temperature")
parser.add_argument('com',  help = "Enter com port")
parser.add_argument('address',  help ="Enter address for the S2M64",type=int)
parser.add_argument('channel', help="Enter channel", type=int)
parser.add_argument('mode', help="Enter UT for ultrasound or T for temperature")
args = parser.parse_args()
#print(args)
um = um_func.SwarmActions()


# setup of serial port
# the s2m64 uses rs485 2wire
def setup_serial(port):
    #print(port)
    port = port
    ser = serial.Serial(port)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.xonxoff = False
    return ser

# if there is no connected transducer the meas_calc value will stay below 10 000
# if the transducer is connected and shots against air or simular, it will show somewhere around 37k
# if a finger is presset against the sensor, it will some something around 14-26k. you have to have a bit of pressure to ensure proper contact gainst a finger
# channels are from 1-64
def measure_ut(ser, add, channel):
    meas = um.measure_us_custom(ser, add, channel,  0, 200, 1024, 0, 99, 200, 0, 2, 5)
    meas_calc = (np.sum(np.abs(meas)))/20
    print(meas_calc)
    if meas_calc < 18000 and meas_calc > 10000:
        status = 'OK'
        print(status)
    else:
        status = 'FAIL'
        print(status)
    return status

# channels are from 0-15
def measure_t(ser,add, channel):
    meas = um.measure_temperature_ext(ser,add,channel)
    print(meas)
    return meas

ser = setup_serial(args.com)
if args.mode.upper() == 'UT':
    measure_ut(ser,args.address, args.channel)
if args.mode.upper() == 'T':
    measure_t(ser,args.address, args.channel)
