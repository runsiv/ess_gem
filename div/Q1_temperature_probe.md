---
###Title:Q1_temperature_probe 
---
 ## How to use it
"python Q1_temperature_probe.py "comport" "S2M64 address" "Number of temp sensors" "logging interval"
## Example:
"python .\Q1_temperature_probe.py com6 3 4 2"
## This wil give the following:
serial port = com6

S2M64 address = 3

Number of temp sensor = 4

logging interval = 2 min

## Python packages needed for this work
serial

"pip install pyserial"

numpy

pip install numpy
