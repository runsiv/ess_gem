import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import serial
import time
import threading
from threading import Thread
import concurrent.futures
import um_functions.um_func as um_func
from s2m64_class import S2m64
import logging
import signal
import argparse
import json
import os
import time

parser = argparse.ArgumentParser(description="Script for ESS testing PCEA")
parser.add_argument('sdb1', help='Com port for SDB1')
parser.add_argument('sdb2', help='Com port for SDB2')
parser.add_argument('interval', help='Samling interval in minutes')

args = parser.parse_args()
um = um_func.SwarmActions()
path = os.getcwd()
inputFolder = input("Enter Serial number of PCEA: ")
test_folder = '\\' + inputFolder
new_path = path+test_folder
if not os.path.isdir(new_path):
    os.makedirs(new_path)
os.chdir(new_path)
logging.basicConfig( filename=inputFolder+'.txt', level=logging.ERROR, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )

def create_rev_file():
    rev = {
        'rev':'0.1',
        'release date': '15/11/2019',
        'type': 'pcea'
    }
    filename = 'rev.txt'
    if os.path.isfile('./'+filename)==False:
        with open(filename, 'w') as f:
            f.write(str(rev))


# setup serial port 1
def setup_serial_1(port):
    #port = (input('sdb1: '))
    ser = serial.Serial(port)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.xonxoff = False
    return ser

# setup serial port 2
def setup_serial_2(port):
    #port = (input('sdb2: '))
    ser = serial.Serial(port)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.xonxoff = False
    return ser

# class handling threading of serial buss 1, and serial bus 2
class threaderClass(threading.Thread):
    def __init__(self, threadID,  functioncall, sdb ):
        threading.Thread.__init__(self, daemon=True)
        #super(threaderClass, self).__init__()
        self.threadID = threadID
        self.functioncall = functioncall
        self.sdb = sdb
        self._return = None
        self._stop_event = threading.Event()

    def run(self):
        self._return = self.functioncall(self.sdb)

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def join(self):
        threading.Thread.join(self)
        return self._return


# class for S2m64 with it functions


def create_s2m64_bus1(ser, um, mode, ref, client):
    add = []
    for i in range(1,8,2):
        #add.append(int(input('Address for S2M64 {} for sdb1: '.format(i+1))))
        add.append(i)
    sdb1 = []
    for i in range(len(add)):
        sdb1.append(S2m64(ser,um,add[i], mode, ref, client))
        sdb1[i].setup()
    return sdb1


def create_s2m64_bus2(ser, um, mode, ref, client):
    add = []
    for i in range(2,8,2):
        add.append(i)
        #add.append(int(input('Address for S2M64 {} for sdb2: '.format(i+1))))
    sdb2 = []
    for i in range(len(add)):
        sdb2.append(S2m64(ser,um,add[i], mode, ref,client))
        sdb2[i].setup()
    return sdb2

def measure_sdb(sdb):
    for i in range(len(sdb)):
        sdb[i].meas_cycle()


def update_counter(counter):
    f=open('counter.txt', 'w')
    f.write(str(counter))
    f.close()


if __name__ == '__main__':
    create_rev_file()
    ser_1 = setup_serial_1(args.sdb1)
    ser_2 = setup_serial_2(args.sdb2)
    interval = int(args.interval)
    mode ='0'
    ref = 0
    client = 0
    sdb1 = create_s2m64_bus1(ser_1, um, mode, ref, client )
    sdb2 = create_s2m64_bus2(ser_2, um, mode, ref, client )
    print("Test starting..")
    #logging.log("Test starting..")
    counter = 0
    update_counter(counter)
    thread_1 = threaderClass(1, measure_sdb, sdb1)
    thread_2 = threaderClass(1, measure_sdb, sdb2)
    thread_2.start()
    thread_1.start()
    thread_2.join()
    thread_1.join()



    print('init run done')

    while(1):
        start = datetime.datetime.now()
        next_interval = start + datetime.timedelta(minutes=interval)

        print("Next measurement", next_interval)
        #logging.log("Next measurement ", next_interval)
        while next_interval  > datetime.datetime.now():
            time.sleep(1)
        print( datetime.datetime.now())
        counter = counter + 1
        update_counter(counter)
        thread_1 = threaderClass(1, measure_sdb, sdb1)
        thread_2 = threaderClass(2, measure_sdb, sdb2)


        thread_2.start()
        thread_1.start()
        thread_1.join()
        thread_2.join()
        print( datetime.datetime.now())
        print('done')
    exit()
