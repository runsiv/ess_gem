#!/usr/bin/env python
# -*- coding: iso-8859-1
#
import time
import json
import struct
import logging
import os


class UmFunc:
    this_dir, this_file = os.path.split(__file__)
    jfile = open(os.path.join(this_dir, 'um_protocol.json'))
    um_protocol = json.load(jfile)
    jfile.close()


    # ---------------------------------------------------------------------------------------------
    #
    # Standard 256-entry CCITT-CRC16 lookup table for accelerated CRC calculations
    #
    # ---------------------------------------------------------------------------------------------
    __crcLookupTable__ = (0,  4129,  8258, 12387,
        16516, 20645, 24774, 28903, 33032, 37161, 41290, 45419, 49548, 53677, 57806, 61935,
        4657,    528, 12915,  8786, 21173, 17044, 29431, 25302, 37689, 33560, 45947, 41818,
        54205, 50076, 62463, 58334,  9314, 13379,  1056,  5121, 25830, 29895, 17572, 21637,
        42346, 46411, 34088, 38153, 58862, 62927, 50604, 54669, 13907,  9842,  5649,  1584,
        30423, 26358, 22165, 18100, 46939, 42874, 38681, 34616, 63455, 59390, 55197, 51132,
        18628, 22757, 26758, 30887,  2112,  6241, 10242, 14371, 51660, 55789, 59790, 63919,
        35144, 39273, 43274, 47403, 23285, 19156, 31415, 27286,  6769,  2640, 14899, 10770,
        56317, 52188, 64447, 60318, 39801, 35672, 47931, 43802, 27814, 31879, 19684, 23749,
        11298, 15363,  3168,  7233, 60846, 64911, 52716, 56781, 44330, 48395, 36200, 40265,
        32407, 28342, 24277, 20212, 15891, 11826,  7761,  3696, 65439, 61374, 57309, 53244,
        48923, 44858, 40793, 36728, 37256, 33193, 45514, 41451, 53516, 49453, 61774, 57711,
        4224,    161, 12482,  8419, 20484, 16421, 28742, 24679, 33721, 37784, 41979, 46042,
        49981, 54044, 58239, 62302,   689,  4752,  8947, 13010, 16949, 21012, 25207, 29270,
        46570, 42443, 38312, 34185, 62830, 58703, 54572, 50445, 13538,  9411,  5280,  1153,
        29798, 25671, 21540, 17413, 42971, 47098, 34713, 38840, 59231, 63358, 50973, 55100,
        9939,  14066,  1681,  5808, 26199, 30326, 17941, 22068, 55628, 51565, 63758, 59695,
        39368, 35305, 47498, 43435, 22596, 18533, 30726, 26663,  6336,  2273, 14466, 10403,
        52093, 56156, 60223, 64286, 35833, 39896, 43963, 48026, 19061, 23124, 27191, 31254,
        2801,   6864, 10931, 14994, 64814, 60687, 56684, 52557, 48554, 44427, 40424, 36297,
        31782, 27655, 23652, 19525, 15522, 11395,  7392,  3265, 61215, 65342, 53085, 57212,
        44955, 49082, 36825, 40952, 28183, 32310, 20053, 24180, 11923, 16050,  3793,  7920)

    # ---------------------------------------------------------------------------------------------
    #
    # Calculate CCITT-CRC16 non-reversed CRC
    # Source made from algorithm found in "CRC-16C++ class 1.0" from
    # Programmer's Heaven, program no. 23351, dated 2002-09-10, Public Domain,
    # http://www.programmersheaven.com/zone3/cat415/23351.htm
    #
    # ---------------------------------------------------------------------------------------------
    def um_crc16(self, inbuffer):
        import numpy
        if isinstance(inbuffer, str):
            buffer = numpy.array(bytearray(inbuffer))
        else:
            buffer = numpy.array(bytearray(inbuffer))
        crc = 7439
        for n in range(buffer.size):
            table_index = ((crc >> 8) ^ buffer[n]) & 255
            crc = (self.__crcLookupTable__[table_index]) ^ ((crc << 8) & 65535)
        return crc


    # ---------------------------------------------------------------------------------------------
    #
    # Append CRC checksum to ultramonit telegram
    # Input: list containing an ultramonit telegram without checksum
    # Return: List with telegram including checksum
    #
    # ---------------------------------------------------------------------------------------------
    def um_make_telegram(self, header, data=None):
        if not all(0x00 <= i <= 0xff for i in header):       # all elements must be >= 0 and <=0xff
            logging.error('ERROR: failed to make telegram. Invalid byte value in header:')
            logging.error(header)
            return bytearray(0)
        if len(header) < 14:
            logging.error('ERROR: failed to make telegram. Header is too short:')
            logging.error(header)
            return bytearray(0)
        # Message is valid. Format it.
        header = header[0:14]                       # Toss extra header bytes if present
        crc = self.um_crc16(header)                      # Calculate CRC checksum of header
        header.append(crc >> 8)                   # Append CRC to header
        header.append(crc & 0xff)
        if not data:          # Stop her if message is header only
            return header
        crc = self.um_crc16(data)                        # Calculate CRC checksum of data
        data.append(crc >> 8)                     # Append CRC to header
        data.append(crc & 0xff)
        return header + data                        # Build message with both header and data


    # ---------------------------------------------------------------------------------------------
    # Transmit ultramonit telegram
    # ---------------------------------------------------------------------------------------------
    @staticmethod
    def um_transmit(port, telegram):
        # Flush input & output buffer, discarding all content
        port.flushInput()
        port.flushOutput()
        # Send message. Add 0xFF preamble to ensure UART synchronization
        port.write(b'\377' + bytes(telegram))

    # ---------------------------------------------------------------------------------------------
    # Transmit ultramonit telegram and receive reply
    # ---------------------------------------------------------------------------------------------
    def um_transmit_receive(self, port, telegram):
        self.um_transmit(port, telegram)
        time.sleep(0.005)
        # Prepare reception of reply
        start = time.clock()
        reply = b''
        bytes_received = 0
        # Keep trying until we succeed or time out
        while (time.clock() - start) < port.timeout:
            # Read enough data to form a Ultramonit message (if not rescanning a data packet)
            if len(reply) < 16:
                bytes_received -= len(reply)
                reply += bytearray(port.read(16 - len(reply)))
                bytes_received += len(reply)
            # Give up trying if read time'd out
            if len(reply) < 16:
                break
            # Toss away bytes until we find the 0xFA 0xCD that marks start of ultramonit message
            while (len(reply) >= 2) and ((reply[0] != 250) or (reply[1] != 205)):
                reply = reply[1:]
            # Do we still have enough for an Ultramonit message, if not loop back to read what is missing
            if len(reply) >= 16:
                # We have enough. Check that message CRC is valid
                if self.um_crc16(reply) == 0:
                    # CRC is good. Check if message has data packet
                    data_size = (reply[12] << 8) + reply[13]                 # calculate length of following datablock
                    if data_size == 0:
                        # No data packet. All is good. Return header packet only
                        return reply
                    else:
                        # Read data packet
                        datapacket = bytearray(port.read(data_size))
                        reply += datapacket
                        bytes_received += len(reply)
                        if len(datapacket) < data_size:                   # check for correct length
                            logging.warning('WARNING: datapackage too short, expected %d bytes', data_size)
                            break
                        if self.um_crc16(datapacket) == 0:                     # Check CRC of data
                            return reply
                        else:
                            # Invalid CRC in data packet. Toss start bytes and keep scanning for a valid message
                            logging.warning('WARNING: datapackage crc error')
                            reply = reply[2:]
                else:
                    # Invalid CRC in header. Toss start bytes and keep scanning for a valid message
                    logging.debug('header crc error')
                    reply = reply[2:]
        # Timed out while scanning received bytes
        logging.warning('WARNING: No valid telegram response. Received  %d bytes before timeout occured', bytes_received)
        return bytearray(0)


    # ---------------------------------------------------------------------------------------------
    # Build ultramonit telegram to send from command name
    # ---------------------------------------------------------------------------------------------
    def um_telegram(self, command, address, channel=0, value=None, data=None):
        pre1 = self.um_protocol['preamble1']
        pre2 = self.um_protocol['preamble2']
        ver = self.um_protocol['version']
        from_adr = self.um_protocol['own_address']
        cmd = self.um_protocol[command]['cmd']
        subcmd = self.um_protocol[command]['subcmd']
        aux = self.um_protocol[command]['aux'] + channel
        if value is None:
            v = struct.pack('>L', 0)
        else:
            aux += 128                          # set bit 7 of aux field if rw=1
            if self.um_protocol[command]['type'] == 'real':
                v = struct.pack('>f', value)
            else:
                v = struct.pack('>L', value)
        if data is None:
            data_length = 0
            datamsg = None
        else:
            data_length = len(data) + 2
            datamsg = data.copy()
        return self.um_make_telegram([pre1, pre2, ver, address, from_adr, cmd, subcmd, aux, v[0], v[1], v[2], v[3], data_length >> 8, data_length & 0xFF], datamsg)


    # ---------------------------------------------------------------------------------------------
    # Decode ultramonit telegram reply
    # ---------------------------------------------------------------------------------------------
    def um_decode_reply(self, command, tlg, address, value, reply, invalid):
        tx_string = "".join("%02x " % b for b in tlg)
        if value is None:
            cmd_string = 'Read {0} from device at address {1}'.format(command, address)
        else:
            cmd_string = 'Set {0} to {1} in device at address {2}'.format(command, value, address)
        if invalid:
            if reply:
                rx_string = "".join("%02x " % b for b in reply)
                logging.warning('WARNING: Unexpected reply on "%s". Message is [%s]. Reply is [%s]', cmd_string, tx_string, rx_string)
            else:
                logging.debug('%s. As expected, no reply on invalid command', cmd_string)
            return
        if len(reply) < 16:
            logging.warning('WARNING: No valid reply on "%s". Message is[%s].', cmd_string, tx_string)
            return
        if self.um_protocol[command]['type'] == 'unsigned':
            value = struct.unpack('>L', reply[8:12])[0]
            logging.debug('%s. Value read: dec. %d, hex 0x%x', cmd_string, value, value)
        elif self.um_protocol[command]['type'] == 'real':
            value = struct.unpack('>f', reply[8:12])[0]
            logging.debug('%s. Value read: "%f"', cmd_string, value)
        elif self.um_protocol[command]['type'] == 'string':
            if len(reply) > 16:
                logging.debug('%s. Value read: "%s"', cmd_string, reply[16:-2])
            else:
                rx_string = "".join("%02x " % b for b in reply)
                logging.warning('WARNING: No reply string on "%s". Message is [%s]. Reply is [%s].', cmd_string, tx_string, rx_string)
        elif self.um_protocol[command]['type'] == 'hexstring':
            if len(reply) > 16:
                rx_string = "".join("%02x " % b for b in reply[16:-2])
                logging.debug('%s. Value read: "%s"', cmd_string, rx_string)
            else:
                rx_string = "".join("%02x " % b for b in reply)
                logging.warning('WARNING: No reply string on "%s". Message is [%s]. Reply is [%s].', cmd_string, tx_string, rx_string)
        elif self.um_protocol[command]['type'] == 'trace':
            if len(reply) > 16:
                trace = struct.unpack('>%sh' % ((len(reply) - 18) // 2), reply[16:-2])
                rx_string = "".join("%d, " % s for s in trace)
                logging.debug('%s. Value read: "%s"', cmd_string, rx_string)
            else:
                rx_string = "".join("%02x " % b for b in reply)
                logging.warning('WARNING: No reply trace on "%s". Message is [%s]. Reply is [%s].', cmd_string, tx_string, rx_string)
        elif self.um_protocol[command]['type'] == 'none':
            logging.debug('%s. Valid reply received', cmd_string)
        else:
            logging.error('ERROR: %s. Unsupported parameter type "%s""', cmd_string, self.um_protocol[command]['type'])


    # ---------------------------------------------------------------------------------------------
    # Function to generate ultramonit telegram, handle transmission and receive the response
    # ----------------------------------------------------------------------------------------------------------------------
    def um_message(self, port, command, address, channel=0, value=None, data=None, invalid=False):
        tlg = self.um_telegram(command, address, channel, value, data)    # build ultramonit command telegram
        if address == 255:
            if value is None:
                logging.error('ERROR: Broadcast "Read %s", this is NOT possible on ultramonit!', command)
            else:
                logging.debug('Broadcasting command "Write %s" to all devices on bus.', command)
                self.um_transmit(port, tlg)
            return None
        else:
            ret = self.um_transmit_receive(port, tlg)
            self.um_decode_reply(command, tlg, address, value, ret, invalid)
            return ret


class SwarmActions(UmFunc):
    """docstring for swarm_actions"""
    def __init__(self):
        self.reply = 0
        self.preamble = 0
        self.version = 0
        self.to_addr = 0
        self.from_addr = 0
        self.cmd = 0
        self.subcmd = 0
        self.aux = 0
        self.parameter = 0
        self.data_length = 0
        self.header_crc = 0
        self.aggregates = 0
        self.datapacket = 0
        self.datapacket_crc = 0
        self.channel = 0


    def measure_temperature_ext(self, port, address, channel):
        """Measure the external temperature if a temperature sensor is attached, currently not supporting multiple sensors
        example "measure_us_temperature_ext(port, address)
        """
        ret = self.um_message(port,'swarm_measure_temp_ext', address,  channel-1)
        return self.decode_reply(ret, 'swarm_measure_temp_ext')

    def measure_temperature_int(self, port, address):
        """Measure the internal temperature
        example "measure_us_temperature_int(port, address)
        """
        return self.read_um_data(port, address, 'swarm_measure_temp_int')

    def measure_us(self, port, address=2):
        return self.read_um_data(port, address, 'swarm_measure_us')

    def measure_acc(self, port, address):
        return self.read_um_data(port, address, 'swarm_measure_acc_all')

    def measure_us_custom(self, port, address, channel, RX_delay, TX_delay, avg, start, stop, boost, rx_mode, pulse, divider):
        """This is the custom measurement function for the S2M64
        the input parameteres are the following (port, address, channel, RX_delay, TX_delay, avg, start, stop, boost, rx_mode, pulse, divider)
        Example: "um.measure_us_custom(sdb1, 1, 1, 0, 100, 1024, 0, 650, 200, 4, 2, 5)"
        """
        #t = port.timeout
        #port.timeout = 5
        configuration = [RX_delay, TX_delay, avg, start, stop, boost, rx_mode, pulse, divider ]
        #configuration = [0, 100, 1024, 0, 650, 250, 4, 2, 5] # RX delay, TX delay, Accumulations, Start, Stop, Boost, RXmode, Pulse, Divider
        config = list(struct.pack('>%sL' % len(configuration), *configuration))
        ret = self.um_message(port, 'swarm_measure_us_custom', address, channel, data=config)
        #port.timeout = t
        return self.decode_reply(ret, 'swarm_measure_us_custom')

    def read_firmware_rev(self, port, address=2):
        """Read the firmware revision of the S2M64
        example: read_firmware_rev(port, address)
        """
        return self.read_um_data(port, address, 'swarm_read_firmware_rev')

    def read_serial(self, port, address=2):
        """Read the serial number of the S2M64
        example: read_serial(port, address)
        """
        return self.read_um_data(port, address, 'swarm_read_serialnumber')

    def read_um_data(self, port, address, command):
        self.__init__()
        ret = self.um_message(port, command, address)
        return self.decode_reply(ret, command)

    def scan_bus(self, port, min_addr=0, max_addr=15):
        ''' Scan the serial bus for Swarms
        example: scan_bus(port, min_add, max_add)
        '''
        # TODO: Make this function scan for only Swarms, not other products (etc. FDL)
        swarms = {}
        for addr in range(min_addr, max_addr+1):
            ret = self.um_message(port, 'swarm_read_serialnumber', addr)
            if  not ret:
                continue
            ret = self.decode_reply(ret)
            if self.datapacket:
                swarms[addr] = self.datapacket
        return swarms

    def set_address(self, port, address, new_address):
        """ Set the address of the S2M64
        Example: "set_address(port, old address, new_address)"""

        return self.set_um_data(port, address, 'swarm_config_rs485_address', value=new_address)

    def set_gain(self, port, address, new_gain):
        return self.set_um_data(port, address, 'swarm_config_ultrasound_divider_exponent', value=new_gain)

    def set_um_data(self, port, address, command, value=None, data=None):
        self.__init__()
        ret = self.um_message(port, command, address, value=value, data=data)
        return self.decode_reply(ret, command)

    def set_baudrate(self, port, address, baudrate):
        """ Set new baudrate for the swarm or S2M64
        exampe set_baudate(port, addresse, verdi)
        """
        return self.set_um_data(port, address, 'swarm_config_rs485_speed', baudrate )

    def swarm_config_factory_reset(self, port, address):

        return self.set_um_data(port, address, 'swarm_config_factory_reset')

    def read_temperature_offset_internal(self, port, address):
        return self.read_um_data(port, address, 'swarm_config_temp_offset_internal')

    def read_temperature_offset(self, port, address):
        return self.read_um_data(port, address, 'swarm_config_temp_offset')

    def write_temperature_offset(self, port, address, new_offset):
        return self.set_um_data(port, address, 'swarm_config_temp_offset', value=new_offset)

    def swarm_config_rs485_reset(self, port, address):
        return self.set_um_data(port,address, 'swarm_config_rs485_reset')

    def swarm_config_ultrasound_s1_reset(self, port, address):
        return self.set_um_data(port,address, 'swarm_config_ultrasound_reset_s1')

    def decode_reply(self, reply, command=None):
        if len(reply) < 14:
            return self.reply
        self.reply = reply
        self.preamble = reply[0:2]
        self.version = reply[2]
        self.to_addr = reply[3]
        self.from_addr = reply[4]
        self.cmd = reply[5]
        self.subcmd = reply[6]
        self.aux = reply[7]
        self.parameter = reply[8:12]
        self.data_length = struct.unpack('>h', reply[12:14])[0]
        self.header_crc = reply[14:16]
        if self.data_length > 0:
            self.datapacket = reply[16:-2]
            self.datapacket_crc = reply[-2:]
        if command is None:
            return self.reply
        if self.um_protocol[command]['type'] == 'unsigned':
            self.aggregates = struct.unpack('>L', reply[8:12])[0]
        elif self.um_protocol[command]['type'] == 'real':
            self.aggregates = struct.unpack('>f', reply[8:12])[0]
        elif self.um_protocol[command]['type'] == 'string':
            self.aggregates = self.datapacket
        elif self.um_protocol[command]['type'] == 'hexstring':
            if len(reply) > 16:
                self.aggregates = "".join("%02x " % b for b in reply[16:-2])
            else:
                self.aggregates = "".join("%02x " % b for b in reply)
        elif self.um_protocol[command]['type'] == 'trace':
            if len(reply) > 16:
                self.aggregates = struct.unpack('>%sh' % ((len(reply) - 18) // 2), reply[16:-2])
            else:
                self.aggregates = "".join("%02x " % b for b in reply)
        return self.aggregates

    def calculated_header_crc(self):
        return self.um_crc16(self.reply[0:14]) if self.reply else None

    def calculated_datapacket_crc(self):
        return self.um_crc16(self.reply[16:-2]) if self.data_length > 0 else None
