import sys
sys.path.insert(0, r"C:/Users/mengesvold/Documents/Bitbucket/") # Mappe hvor um_functions ligger
import um_functions.um_func as um_func
import matplotlib.pyplot as plt
import serial
import logging

'''
 	Set logging level
	Level		Numeric value
	CRITICAL	50
	ERROR		40
	WARNING		30
	INFO		20
	DEBUG		10
	NOTSET		0
'''
logging.basicConfig( level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )

# Åpne serieport
portname = 'COM6'
address = 3			# Swarm address 

port = serial.Serial( portname )                # open serial port
port.close()
port.open()
port.baudrate = 115200
port.timeout = 0.5
port.xonxoff = False

um = um_func.SwarmActions()

trace = um.measure_us(port, address)

plt.plot(trace)
plt.show()