#!/usr/bin/env python
# -*- coding: iso-8859-1

import serial
import time
from um_func import *           # import misc functions from the file um_func.py
import logging

# ------------------------ Initialization

portname = 'COM6'
address = 3
serialnr = '2018-00001'
#logging.basicConfig( filename='logfile.txt', level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )
logging.basicConfig( level=logging.DEBUG, format='%(asctime)s %(message)s', datefmt='%H:%M:%S' )

logging.info('Opening serial port %s', portname)
port = serial.Serial( portname )                # open serial port
port.baudrate = 115200
port.timeout = 0.5
port.xonxoff = False

logging.info('Starting test.')

# ------------------------ Reader functions
def read_event_counters( a = address ):
	# Read event counters
	um_message( port, 'swarm_evcnt_msg_received', a )
	um_message( port, 'swarm_evcnt_broadcast_received', a )
	um_message( port, 'swarm_evcnt_responce_sent', a )
	um_message( port, 'swarm_evcnt_bad_data_block', a )
	um_message( port, 'swarm_evcnt_bad_header', a )
	um_message( port, 'swarm_evcnt_unknown_cmd', a )
	um_message( port, 'swarm_evcnt_illegal_write', a )
	um_message( port, 'swarm_evcnt_illegal_read', a )
	um_message( port, 'swarm_evcnt_ext_temperature_read', a )
	um_message( port, 'swarm_evcnt_ext_temperature_error', a )
	um_message( port, 'swarm_evcnt_int_temperature_read', a )
	um_message( port, 'swarm_evcnt_int_temperature_error', a )
	um_message( port, 'swarm_evcnt_accelerometer_read', a )
	um_message( port, 'swarm_evcnt_accelerometer_error', a )
	um_message( port, 'swarm_evcnt_ultrasound_read', a )
	um_message( port, 'swarm_evcnt_boost_failure', a )
	um_message( port, 'swarm_evcnt_fpga_boot_error', a )
	um_message( port, 'swarm_evcnt_fpga_read_error', a )
	um_message( port, 'swarm_evcnt_fpga_write_error', a )
	um_message( port, 'swarm_evcnt_fpga_data_error', a )
	um_message( port, 'swarm_evcnt_eeprom_write', a )
	um_message( port, 'swarm_evcnt_minmax_adjust', a )
	um_message( port, 'swarm_evcnt_factory_reset', a )

def read_identity( a = address ):
	um_message( port, 'swarm_read_serialnumber', a )
	um_message( port, 'swarm_read_chip_id', a )
	um_message( port, 'swarm_read_production_time', a )
	um_message( port, 'swarm_read_firmware_rev', a )
	um_message( port, 'swarm_read_product_type', a )
	um_message( port, 'swarm_read_hardware_revision', a )
	um_message( port, 'swarm_read_fpga_revision', a )

def read_accelerometer( a = address ):
	um_message( port, 'swarm_measure_acc_all', a )
	um_message( port, 'swarm_measure_acc_x', a )
	um_message( port, 'swarm_measure_acc_y', a )
	um_message( port, 'swarm_measure_acc_z', a )

def read_config_ultramonit( a = address ):
	um_message( port, 'swarm_config_rs485_address', a )
	um_message( port, 'swarm_config_rs485_speed', a )
	um_message( port, 'swarm_config_rs485_turnaround', a )
	
def read_config_temperature( a = address, channels = 16 ):
	um_message( port, 'swarm_config_temp_offset_internal', a )  
	for i in range(channels):
		um_message( port, 'swarm_config_temp_offset', a, channel = i )
	um_message( port, 'swarm_config_temp_averages', a )

def read_config_ultrasound( a = address ):
	um_message( port, 'swarm_config_ultrasound_sample_delay', a )
	um_message( port, 'swarm_config_ultrasound_transmit_delay', a )
	um_message( port, 'swarm_config_ultrasound_accumulations', a )
	um_message( port, 'swarm_config_ultrasound_data_start', a )
	um_message( port, 'swarm_config_ultrasound_data_stop', a )
	um_message( port, 'swarm_config_ultrasound_boost', a )
	um_message( port, 'swarm_config_ultrasound_rx_mode', a )
	um_message( port, 'swarm_config_ultrasound_tx_pulse', a )
	um_message( port, 'swarm_config_ultrasound_divider_exponent', a )

def read_config_location( a = address ):
	um_message( port, 'swarm_config_location_swarmID', a )
	um_message( port, 'swarm_config_location_GPS_pos', a )
	um_message( port, 'swarm_config_location_name', a )
	um_message( port, 'swarm_config_location_grid_position', a )
	um_message( port, 'swarm_config_location_grid_size', a )
	um_message( port, 'swarm_config_location_pipe_size', a )
	um_message( port, 'swarm_config_location_min_wall_thickness', a )
	um_message( port, 'swarm_config_location_best_algorithm', a )
	um_message( port, 'swarm_config_location_min_signal_amp', a )

def read_config_all( a = address ):
	read_config_ultramonit( a )
	read_config_temperature( a )
	read_config_ultrasound( a )
	read_config_location( a )

def read_temperature( a = address, channels = 16 ):
	um_message( port, 'swarm_measure_temp_int',  a )
	for i in range(channels):
		um_message( port, 'swarm_measure_temp_ext',  a, channel = i )

def read_ultrasound( a = address, channels = 65 ): # Also test 'open' channel
	t = port.timeout
	port.timeout = 5.0
	configuration = [ 0, 100, 1024, 0, 1024, 250, 4, 2, 5 ] # RX delay, TX delay, Accumulations, Start, Stop, Boost, RXmode, Pulse, Divider
	config = list( struct.pack( '>%sL' % len(configuration), *configuration ) )
	for i in range(channels):  
		um_message( port, 'swarm_measure_us',  a, channel = i )
		um_message( port, 'swarm_measure_us_custom',  a, channel = i, data = config )
	port.timeout = t

def read_all( a = address ):
	read_event_counters( a )
	read_identity( a )
	read_accelerometer( a )
	read_config_all( a )
	read_accelerometer( a )
	read_temperature( a )
	read_ultrasound( a )

# ------------------------ Writer functions

def write_config_ultramonit(  a = address, reset = True, swap = False, serial = serialnr ):
	if swap:
		# Swap node address to another one and back to the original one
		um_message( port, 'swarm_config_rs485_address', a, value = a +10 )
		um_message( port, 'swarm_config_rs485_address', a + 10, value = a )
		# Swap speed to 57600 and back to default
		um_message( port, 'swarm_config_rs485_speed', a, value = 8 )
		port.baudrate = 57600
		um_message( port, 'swarm_config_rs485_speed', a, value = 10 )
		port.baudrate = 115200
	um_message( port, 'swarm_config_rs485_turnaround', a, value = 5 )
	# Read them back
	read_config_ultramonit( a = a )
	if reset:
		# Send "reset all" command
		um_message( port, 'swarm_config_rs485_reset', a, value = 0 )
		# Use keyed broadcast to set node address after factory reset
		um_message( port, 'swarm_config_rs485_address', 255, value = a, data = [ ord( i ) for i in list(serial) ] )
		time.sleep( 0.1 )
		# Read them back to verify reset
		read_config_ultramonit( a = a )
		

def write_config_temperature( a = address, channels = 16, reset = True ):
	# Set temperature configuration parameters 
	um_message( port, 'swarm_config_temp_offset_internal', a, value = 17.0 )  
	for i in range(channels):
		um_message( port, 'swarm_config_temp_offset', a, channel = i, value = i * 1.0 )
	um_message( port, 'swarm_config_temp_averages', a, value = 300 )
	# Read them back
	read_config_temperature( a = a, channels = channels )
	if reset:
		# Send "reset temperature" command
		um_message( port, 'swarm_config_temp_reset', a, value = 0 )
		# Read them back to verify reset
		read_config_temperature( a = a, channels = channels )
		
def write_config_ultrasound( a = address, reset = True ):
	um_message( port, 'swarm_config_ultrasound_sample_delay', a, value = 10 )
	um_message( port, 'swarm_config_ultrasound_transmit_delay', a, value = 11 )
	um_message( port, 'swarm_config_ultrasound_accumulations', a, value = 12 )
	um_message( port, 'swarm_config_ultrasound_data_start', a, value = 13 )
	um_message( port, 'swarm_config_ultrasound_data_stop', a, value = 14 )
	um_message( port, 'swarm_config_ultrasound_boost', a, value = 15 )
	um_message( port, 'swarm_config_ultrasound_rx_mode', a, value = 16 )
	um_message( port, 'swarm_config_ultrasound_tx_pulse', a, value = 17 )
	um_message( port, 'swarm_config_ultrasound_divider_exponent', a, value = 18 )
	# Read them back
	read_config_ultrasound( a = a )
	if reset:
		# Send "reset ultrasound" command
		um_message( port, 'swarm_config_ultrasound_reset', a, value = 0 )
		# Read them back to verify reset
		read_config_ultrasound( a = a )

def write_config_location( a = address, reset = True ):
	# Set location configuration parameters
	um_message( port, 'swarm_config_location_swarmID', a, value = 17 )
	um_message( port, 'swarm_config_location_GPS_pos', a, value = 1234567 )
	um_message( port, 'swarm_config_location_name', a, value = 0, data = [ ord( i ) for i in list( 'It''s right next to me' ) ] )
	um_message( port, 'swarm_config_location_grid_position', a, value = 4443322 )
	um_message( port, 'swarm_config_location_grid_size', a, value = 31 )
	um_message( port, 'swarm_config_location_pipe_size', a, value = 41 )
	um_message( port, 'swarm_config_location_min_wall_thickness', a, value = 19 )
	um_message( port, 'swarm_config_location_best_algorithm', a, value = 2 )
	um_message( port, 'swarm_config_location_min_signal_amp', a, value = 500 )
	# Read them back
	read_config_location( a = a )
	if reset:
		# Send "reset location" command
		um_message( port, 'swarm_config_location_reset', a, value = 0 )
		# Read them back to verify reset
		read_config_location( a = a )

def write_config_all( a = address, serial = serialnr ):
	write_config_ultramonit( a = a, reset = False )
	write_config_temperature( a = a, reset = False )
	write_config_ultrasound( a = a, reset = False )
	write_config_location( a = a, reset = False )
	um_message( port, 'swarm_config_factory_reset', a, value = 0 )
	um_message( port, 'swarm_config_rs485_address', 255, value = a, data = [ ord( i ) for i in list(serial) ] )
	read_config_all( a = a )

# ------------------------ Utility functions

def test_power_control( a = address ):
	um_message( port, 'swarm_pwrctrl_3v3', a, value = 1 )
	um_message( port, 'swarm_pwrctrl_3v3', a )
	um_message( port, 'swarm_pwrctrl_3v3', a, value = 0 )
	um_message( port, 'swarm_pwrctrl_reset', a )
#	um_message( port, 'swarm_pwrctrl_reset', a, value = 1 )
#	time.sleep( 0.5 )
#	um_message( port, 'swarm_jump_to_bootloader', a, value = 0 )
#	time.sleep( 0.5 )

def test_invalid_messages( a = address ):
	bad_serial= [ ord( i ) for i in list('2018-0000X') ]
	# Check message with invalid address
	um_message( port, 'swarm_evcnt_msg_received', a + 1, invalid = True )
	# Check keyed messages with invalid serialnumber
	um_message( port, 'swarm_config_rs485_address', a, value = a + 1, data = bad_serial, invalid = True )
	um_message( port, 'swarm_config_rs485_speed', a, value = 8, data = bad_serial, invalid = True )
	um_message( port, 'swarm_jump_to_bootloader', a, value = 0, data = bad_serial, invalid = True )
	um_message( port, 'swarm_config_factory_reset', a, value = 0, data = bad_serial, invalid = True )
	# Invalid aux value should cause no reply
	um_message( port, 'swarm_config_rs485_address', a, channel = 8, invalid = True )
	um_message( port, 'swarm_evcnt_msg_received', a, channel = 30, invalid = True )
	# Invalid read & write should cause no reply
	um_message( port, 'swarm_config_factory_reset', a, invalid = True )
	um_message( port, 'swarm_evcnt_msg_received', a, value = 0, invalid = True )
	# Should still reply on old address and baudrate
	um_message( port, 'swarm_config_rs485_address', a )

def test_all():
	read_all()
	write_config_ultramonit( swap = True )
	write_config_temperature()
	write_config_ultrasound()
	write_config_location()
	write_config_all()
	test_power_control()
	test_invalid_messages()
