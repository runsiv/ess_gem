import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import time
import logging
import signal
import argparse
import json
import os
import time
import csv
import glob
import shutil
import tarfile
import multiprocessing.dummy as mp
from multiprocessing import Process
from threading import Thread
from multiprocessing import Pool
from tkinter import filedialog
from tkinter import *
import pdfkit

root = Tk()
root.withdraw()
folder_selected = filedialog.askdirectory()
#test_folder = '\\test_data'
os.chdir(folder_selected)

def find_databases():
    listofdb = []
    for files in glob.glob("*.db"):
        listofdb.append(files)
        # print(files)
    return listofdb



class prosess_data:
    def __init__(self, db):
        self.cur = None
        self.conn = None
        self.numberofmeas = None
        self.number_of_us_passed = None
        self.number_of_us_failed = None
        self.number_of_temp_failed = None
        self.number_of_temp_passed = None
        self.db = db
        self.sn = db[:-3]
        self.passed = None
        self.summary = None
        self.csv = None
        self.df = pd.DataFrame()
        
        self.temp_std = None
        self.us_std = None 
        self.us_mean = None
        self.us_min = None 
        self.us_max = None 
        self.temp_mean = None
        self.temp_min = None 
        self.temp_max = None

    def connect_to_db(self):
        # print(self.db)
        try:
            self.conn = sqlite3.connect(self.db)
            self.cur = self.conn.cursor()
        except Error as e:
            print(e)
        print(self.sn)

   

    # Summary of all temperature channels, calculates min, max and mean of all data
    def temperature_std(self):
        df = pd.read_sql_query("Select * from Temperature", self.conn)
        df = df[df.columns.drop(list(df.filter(regex='_state')))]
        df = df[df.columns.drop('Timestamp')]
        #df = df[df.columns.drop('Serial_Number')]
        df = df[df.columns.drop('index')]


        temp_mean = df.mean()
        self.temp_mean = temp_mean.mean()

        t_std = df.std()
        self.temp_std = t_std.std()

        t_min = df.min()
        self.temp_min = t_min.min()

        t_max = df.max()
        self.temp_max = t_max.max()

        

    # Summary of all ultrasound channels, calculates min, max and mean of all data
    def ultrasound_std(self):
        df = pd.read_sql_query("Select * from Ultrasound", self.conn)
        df = df[df.columns.drop(list(df.filter(regex='_state')))]
        df = df[df.columns.drop('Timestamp')]
        #df = df[df.columns.drop('Serial_Number')]
        df = df[df.columns.drop('index')]
        if 'Address' in df:
            df = df[df.columns.drop('Address')]

        us_mean = df.mean()
        self.us_mean = us_mean.mean()

        us_std = df.std()
        self.us_std = us_std.std()

        us_min = df.min()
        self.us_min = us_min.min()

        us_max = df.max()
        self.us_max = us_max.max()

         



    def number_of_measurements(self):
        self.cur.execute('''SELECT COUNT(*) FROM Temperature''')
        count = self.cur.fetchall()
        self.numberofmeas = int(count[0][0])


    def look_for_fail_in_temperature(self):
        temp_channel_status = []
        ch_list = []
        counter = 0
        temp_len = 33

        for i in range(1, temp_len):
            ch = 'T{}'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Temperature Status where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            if state[0] == ((0),):
                temp_channel_status.append('Pass')
            else:
                temp_channel_status.append(state[0])
                counter = counter + 1

        self.number_of_temp_passed = 32 - counter
        self.number_of_temp_failed = counter
        # return Temperature_status

    def look_for_fail_in_ultrasound(self):
        us_channel_status = []
        ch_list = []
        counter = 0
        ut_len = 33
        for i in range(1, ut_len):
            ch = 'UT{}'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Ultrasound Status where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            # print(state)
            # print(type(state))
            if state[0] == ((0),):
                us_channel_status.append('Pass')
            else:
                us_channel_status.append(state[0])
                counter = counter + 1
        self.number_of_us_failed = counter
        self.number_of_us_passed = 32 - counter

    def create_summary(self):
        if self.number_of_us_passed == 32  and self.number_of_temp_passed == 32:
            self.passed = 'Pass'
        else:
            self.passed = 'Fail'
        self.summary = {
            "System ": self.sn,
            "Status ": self.passed,
            "Meas_Cycles_Done ": self.numberofmeas,
            "UT_Ch_Passed ": self.number_of_us_passed,
            "UT_Ch_Failed ": self.number_of_us_failed,
            "T_Ch_Passed ": self.number_of_temp_passed,
            "T_Ch_Failed ": self.number_of_temp_failed,
            "T_Min_[deg_C]" : self.temp_min,
            "T_Max_[deg_C]" : self.temp_max,
            "T_Avg_[deg_C]" : self.temp_mean,
            #"STD_Temp_[tempC]": self.temp_std, 
            "UT_Min_[Energy]" : self.us_min, 
            "UT_Max_[Energy]" : self.us_max,
            "UT_Avg_[Energy]": self.us_mean}
        #print(self.summary)
        return self.summary
        
    def check_database(self):
        self.connect_to_db()
        self.number_of_measurements()
        self.look_for_fail_in_ultrasound()
        self.look_for_fail_in_temperature()
        self.temperature_std()
        self.ultrasound_std()
        self.create_summary()
        return self.summary

def summary_db(df):
    try:
        conn = sqlite3.connect('summary.db')
    except Error as e:
        print(e)
    try:
        df.to_sql('Summary', con=conn, if_exists='append')
    except Error as e:
        print(e)




def highligth_pass(data):
    '''
    hightlight pass as green
    '''
    color = 'black'
    if data =='Pass':
        color ='green'
    if data =='Fail':
        color ='red'


    return 'color: %s' % color

def type_of_test():
    function_test = "This table shows a summary of the measurement results from the functional test performed prior to applying environmental stress. A functional test is defined as the performance of one measurements cycle."
    cycle_test = "This table shows a summary of the functional testing measurement results during thermal cycling. Functional test during thermal cycling is defined as the performance of at least one measurements cycle during thermal cycling between minimum and maximum design temperature. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    burnin = "This table shows a summary of the functional testing measurement results burn-in. Functional test during burn-in is defined as the performance of at least one measurements cycle during burn-in at maximum design temperature for 48 hours. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    list_text = " Type of test : \n 1. Function Test \n 2. Cycle Test  \n 3. Post Function Test  \n 4. Burn-in: \n 5. Vibration  \n 6. Pressure \n Enter Test number: " 
    vibration = "This table shows a summary of the functional testing measurement results vibration. Functional test during vibration is defined as the performance of at least one measurements cycle during random vibration for 10 minutes. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    pressure = "This table shows a summary of the functional testing measurement results pressure. Functional test during pressure is defined as the performance of at least one measurements cycle during pressure testing. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed." 
    post_function= "This table shows a summary of the measurement results from the functional test performed after applying environmental stress. A functional test is defined as the performance of one measurements cycle."
    tmp = 'This table shows a summary of the measurement results during the test. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed. Result of test: All measurements were recorded and approved according to the test criteria.'
    ans = input((list_text))
    ans = int(ans)
    
    switcher= {
        1: function_test,
        2: cycle_test,
        3: post_function,
        4: burnin,
        5: vibration,
        6: pressure

    }
    s_text = switcher.get(ans, 'invalid type')
    return s_text



def create_attachment_summary(df):
    """
    for files in glob.glob("* Summary.csv"):
        df = pd.read_csv(files)
    df_summary = df[df.columns[1:]].copy(deep=True)"""
    df_summary = df
    sn = input('Enter EUT serial number: ')
    df_name = "Canister Internal Frame " + sn
    text =  type_of_test()   
    #print(df_summary)

    with pd.option_context('display.precision', 5):
                    df_html = (df_summary.style
                               .applymap(highligth_pass)
                               .set_caption(text)
                               .hide_index()
                               .set_table_attributes('class="pure-table"'))
    #df_name=(files[:-7])
    

    pdf_gen(df_name, df_html)

def pdf_gen(df_name, df_html):

    #header = df_name[:-5]
    header = df_name
    css = 'pdf.css'
    options = {
        'page-size': 'A4',
        'orientation': 'Landscape',
        'user-style-sheet': css,
        'header-center': header
    }
    pdfkit.from_string(df_html.render(), header+'.pdf', options=options, css=css)


if __name__ == "__main__":
    
    #test_object = read_rev()
    a = find_databases()
    df = pd.DataFrame()

    tests = []
    for i in range((len(a))):
        tests.append(prosess_data(a[i]))
        output = tests[i].check_database()
        #tests[i].export_databases()
        df = df.append([output], ignore_index=True)
        #print(df)

    summary_db(df)
    #export_db_summary()

    create_attachment_summary(df)