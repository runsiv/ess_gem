---
###Title:Generate_attachment_internal_frame 
---
 ## How to use it:

1. Copy attachments made by running internal_frame.py
2. Make sure pdf.css is the same folder as the raw files
3. run python generate_attachment_internal_frame.py in a terminal
4. Select the folder where the pdf from point 1. is stored
5. Enter correct serial number
6. Rename output pdf


