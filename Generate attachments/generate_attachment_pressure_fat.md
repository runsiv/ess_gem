---
###Title:Generate_attachment 
---
 ## How to use it:

1. Make sure pdf.css is the same folder as the raw files
2. Run python generate_attachment_pressure_FAT.py in a terminal
3. Select the folder where the raw files are stored
4. Enter serial number of the Transducer holder
5. Rename the output pdf

