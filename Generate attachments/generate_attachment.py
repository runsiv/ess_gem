import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import sys
import glob
import pathlib
import concurrent.futures
from zipfile import ZipFile
import os
import csv
import pdfkit
import multiprocessing.dummy as mp
from multiprocessing import Process
from threading import Thread
from multiprocessing import Pool
import seaborn as sns
#css = 'pdf.css'
from tkinter import filedialog
from tkinter import *
root = Tk()
root.withdraw()
folder_selected = filedialog.askdirectory()


def change_folder():
    os.chdir(folder_selected)


def find_databases():
    listofdb = []
    for files in glob.glob("*.db"):
        listofdb.append(files)
        # print(files)
    return listofdb

"""
Class for reading the databases and export it to csv
"""
class prosess_data:
    def __init__(self, db, test_object):
        self.cur = None
        self.conn = None
        self.numberofmeas = None
        self.number_of_us_passed = None
        self.number_of_us_failed = None
        self.number_of_temp_failed = None
        self.number_of_temp_passed = None
        self.db = db
        self.sn = db[:-3]
        self.passed = None
        self.summary = None
        self.csv = None
        self.df = pd.DataFrame()
        self.test_object = test_object
        self.temp_std = None
        self.us_std = None 
        self.us_mean = None
        self.us_min = None 
        self.us_max = None 
        self.temp_mean = None
        self.temp_min = None 
        self.temp_max = None
        self.add = None
        self.ut_len = None
        self.temp_len = None

    def connect_to_db(self):
        # print(self.db)
        try:
            self.conn = sqlite3.connect(self.db)
            self.cur = self.conn.cursor()
        except Error as e:
            print(e)
        print(self.sn)

    def export_db_us(self):
        f = open("S2M64 - " + self.sn+(' Ultrasound Test Data.csv'), 'w')
        self.cur.execute('select * from Ultrasound')
        rows = self.cur.fetchall()
        column_names = [i[0] for i in self.cur.description]
        files = csv.writer(f, lineterminator='\n')
        files.writerow(column_names)
        files.writerows(rows)

        f.close()

    # Summary of all temperature channels, calculates min, max and mean of all data
    def temperature_std(self):
        df = pd.read_sql_query("Select * from Temperature", self.conn)
        df = df[df.columns.drop(list(df.filter(regex='_state')))]
        df = df[df.columns.drop('Timestamp')]
        df = df[df.columns.drop('Serial_Number')]
        df = df[df.columns.drop('index')]


        temp_mean = df.mean()
        self.temp_mean = temp_mean.mean()

        t_std = df.std()
        self.temp_std = t_std.std()

        t_min = df.min()
        self.temp_min = t_min.min()

        t_max = df.max()
        self.temp_max = t_max.max()

        

    # Summary of all ultrasound channels, calculates min, max and mean of all data
    def ultrasound_std(self):
        df = pd.read_sql_query("Select * from Ultrasound", self.conn)
        df = df[df.columns.drop(list(df.filter(regex='_state')))]
        df = df[df.columns.drop('Timestamp')]
        df = df[df.columns.drop('Serial_Number')]
        df = df[df.columns.drop('index')]
        if 'Address' in df:
            df = df[df.columns.drop('Address')]

        us_mean = df.mean()
        self.us_mean = us_mean.mean()

        us_std = df.std()
        self.us_std = us_std.std()

        us_min = df.min()
        self.us_min = us_min.min()

        us_max = df.max()
        self.us_max = us_max.max()
        #self.add = us

    def export_db_temp(self):
        f = open("S2M64 - " +  self.sn+(' Temperature Test Data.csv'), 'w')
        self.cur.execute('select * from Temperature')
        rows = self.cur.fetchall()
        column_names = [i[0] for i in self.cur.description]
        files = csv.writer(f, lineterminator='\n')
        files.writerow(column_names)
        files.writerows(rows)

        f.close()
 
    def number_of_measurements(self):
        self.cur.execute('''SELECT COUNT(*) FROM Temperature''')
        count = self.cur.fetchall()
        self.numberofmeas = int(count[0][0])


    def look_for_fail_in_temperature(self):
        temp_channel_status = []
        ch_list = []
        counter = 0
        
        if self.test_object == 'transducer_holder':
            self.temp_len=5
        if self.test_object == 'transducer_cassett_retro':
            self.temp_len=2
        if self.test_object == 'transducer_cassett_bp':
            self.temp_len=1

        if self.test_object =='pcea':
            self.temp_len=16

        if self.test_object =="int_frame":
            self.temp_len=16

        for i in range(1, self.temp_len):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Temperature where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            if state[0] == ((0),):
                temp_channel_status.append('Pass')
            else:
                temp_channel_status.append(state[0])
                counter = counter + 1
                print("error tmp")
        if self.test_object == 'transducer_holder':
            self.number_of_temp_passed = 5 - counter
            self.number_of_temp_failed = counter
        if self.test_object =="int_frame":
            self.number_of_temp_passed = 16 - counter
            self.number_of_temp_failed = counter
        if self.test_object =='pcea':
            self.number_of_temp_passed = 16 - counter
            self.number_of_temp_failed = counter
        if self.test_object =='transducer_cassett_retro':
            self.number_of_temp_passed = 2 - counter
            self.number_of_temp_failed = counter
        if self.test_object =='transducer_cassett_bp':
            self.number_of_temp_passed = 1 - counter
            self.number_of_temp_failed = counter
        # return Temperature_status

    def look_for_fail_in_ultrasound(self):
        us_channel_status = []
        ch_list = []
        counter = 0
        if self.test_object == 'transducer_holder':
            self.ut_len=35
        if self.test_object =='pcea':
            self.ut_len=64
        if self.test_object =='int_frame':
            self.ut_len=32
        if self.test_object =='transducer_cassett_retro':
            self.ut_len=16
        if self.test_object =='transducer_cassett_bp':
            self.ut_len=15
        for i in range(1, self.ut_len):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Ultrasound where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            # print(state)
            # print(type(state))
            if state[0] == ((0),):
                us_channel_status.append('Pass')
            else:
                us_channel_status.append(state[0])
                counter = counter + 1
                print("error pcea")
        if self.test_object == 'pcea':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 64 - counter
        if self.test_object == 'transducer_holder':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 35 - counter
        if self.test_object =='pcea':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 64 - counter
        if self.test_object =='int_frame':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 32 - counter
        if self.test_object =='transducer_cassett_retro':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 16 - counter
        if self.test_object =='transducer_cassett_bp':
            self.number_of_us_failed = counter
            self.number_of_us_passed = 15 - counter
        




    def create_summary(self):
        print(self.number_of_us_passed)
        if self.number_of_us_passed == self.ut_len and self.number_of_temp_passed == self.temp_len :
            self.passed = 'Pass'
        else:
            self.passed = 'Fail'
        self.summary = {
            "SN ": self.sn,
            "Status ": self.passed,
            "Meas_Cycles_Done ": self.numberofmeas,
            "UT_Ch_Passed ": self.number_of_us_passed,
            "UT_Ch_Failed ": self.number_of_us_failed,
            "T_Ch_Passed ": self.number_of_temp_passed,
            "T_Ch_Failed ": self.number_of_temp_failed,
            "T_Min_[deg_C]" : self.temp_min,
            "T_Max_[deg_C]" : self.temp_max,
            "T_Avg_[deg_C]" : self.temp_mean,
            #"STD_Temp_[tempC]": self.temp_std, 
            "UT_Min_[Energy]" : self.us_min, 
            "UT_Max_[Energy]" : self.us_max,
            "UT_Avg_[Energy]": self.us_mean}
        #print(self.summary)
        return self.summary

    def check_database(self):
        self.connect_to_db()
        self.number_of_measurements()
        self.look_for_fail_in_ultrasound()
        self.look_for_fail_in_temperature()
        self.temperature_std()
        self.ultrasound_std()
        self.create_summary()
        return self.summary

    def export_databases(self):
        self.export_db_us()
        self.export_db_temp()
        # self.export_db_summary()
  


def summary_db(df):
    try:
        conn = sqlite3.connect('summary.db')
    except Error as e:
        print(e)
    try:
        df.to_sql('Summary', con=conn, if_exists='append')
    except Error as e:
        print(e)


def export_db_summary():
    try:
        conn = sqlite3.connect('summary.db')
        cur = conn.cursor()
    except Error as e:
        print(e)
    sn = input('Enter EUT serial number: ')
    filname = test_object.upper() + ' ' +sn + " Summary.csv"
    f = open(filname, 'w')
    cur.execute('select * from Summary')
    rows = cur.fetchall()
    column_names = [i[0] for i in cur.description]
    files = csv.writer(f, lineterminator='\n')
    files.writerow(column_names)
    files.writerows(rows)

    f.close()

# compresses all .csv files found in the directory


def zip_csv():
    raw_files = []
    for files in glob.glob("*.csv"):
        raw_files.append(files)

    with ZipFile('csv_files.zip', 'w') as zip:
        for file in raw_files:
            zip.write(file)
    for file in raw_files:
        try:
            os.remove(file)
        except:
            pass


def zip_json():
    raw_files = []
    for files in glob.glob("*.json"):
        raw_files.append(files)

    with ZipFile('json_files.zip', 'w') as zip:
        for file in raw_files:
            zip.write(file)
    for file in raw_files:
        try:
            os.remove(file)
        except:
            pass


def create_attachment_ut():
    csv_files = []
    for files in glob.glob("*.csv"):
        csv_files.append(files)
        create_df(files)


def highligth_pass(data):
    '''
    hightlight pass as green
    '''
    color = 'black'
    if data =='Pass':
        color ='green'
    if data =='Fail':
        color ='red'


    return 'color: %s' % color

def create_df(files):
    df = pd.read_csv(files)
    df_count = len(df.columns)
    df_row_count = df.shape[0]
    #row_divider_counter = int(df_row_count/29)
    #print(df_count)
    if df_count == 13:
        loop_length = 1
    else:
        loop_length = int((df_count-3)/32)
    df_start = df[df.columns[1:3]].copy(deep=True)

    """
    The exported csv files are splited into multiple dataframes which again is exported to html
    This is done in order to remove lines crossing into oneanother when the html table is spanning multiple lines
    df_start contains the timestamp and serial number while df_subset
    df_tmp contains the n-frist 32 colomns
    df_subset containts the n-first 28 rows of df_tmp
    df_sub_start contains the n-first 28 rows of df_start

    """
    for i in range(loop_length):

        df_tmp = df[df.columns[3+(i*32):35+(i*32)]].copy(deep=True)
        chunksize = 23
        start = 0
        counter = 0

        for start in range(0, df_row_count, chunksize):
            counter = 1 + counter
            df_sub_start = df_start.iloc[start:start + chunksize]
            df_subset = df_tmp.iloc[start:start + chunksize]

            df_csv = df_sub_start.join(df_subset)
            df_csv.style.hide_index()

            if i ==0:
                df_str = files[:-14] + ' Measurements, Channel ' + str(1) + " to " + str(
                    16+(i*16)) + ", page " + str(counter) + " of " + str((int((df_row_count/2)+1)))
            else:
                df_str = files[:-14] + ' Measurements, Channel ' + str(1+(i*16)) + " to " + str(
                    16+(i*16)) + ", page " + str(counter) + " of " + str((int((df_row_count/2)+1)))
            if 'Ultrasound' in df_str:
                caption= df_str[19:] +' [Energy]'
                with pd.option_context('display.precision', 5):
                    df_html = (df_csv.style
                            .applymap(highligth_pass)
                            .set_caption(caption)
                            .set_table_attributes('class="pure-table"'))
            if 'Temperature' in df_str:
                caption= df_str[19:]+' [°C]'
                with pd.option_context('display.precision', 1):
                    df_html = (df_csv.style
                            .applymap(highligth_pass)
                            .set_caption(caption)
                            .set_table_attributes('class="pure-table"'))
           
            pdf_gen(df_str, df_html)

            #df_csv.to_html(df_str + '.html')

def type_of_test():
    function_test = "This table shows a summary of the measurement results from the functional test performed prior to applying environmental stress. A functional test is defined as the performance of one measurements cycle."
    cycle_test = "This table shows a summary of the functional testing measurement results during thermal cycling. Functional test during thermal cycling is defined as the performance of at least one measurements cycle during thermal cycling between minimum and maximum design temperature. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    burnin = "This table shows a summary of the functional testing measurement results burn-in. Functional test during burn-in is defined as the performance of at least one measurements cycle during burn-in at maximum design temperature for 48 hours. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    list_text = " Type of test : \n 1. Function Test \n 2. Cycle Test  \n 3. Post Function Test  \n 4. Burn-in: \n 5. Vibration  \n 6. Pressure \n Enter Test number: " 
    vibration = "This table shows a summary of the functional testing measurement results vibration. Functional test during vibration is defined as the performance of at least one measurements cycle during random vibration for 10 minutes. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed."
    pressure = "This table shows a summary of the functional testing measurement results pressure. Functional test during pressure is defined as the performance of at least one measurements cycle during pressure testing. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. \n The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed." 
    post_function= "This table shows a summary of the measurement results from the functional test performed after applying environmental stress. A functional test is defined as the performance of one measurements cycle."
    tmp = 'This table shows a summary of the measurement results during the test. The table shows the number of measurements recorded and how many have passed or failed the acceptance criteria in the test procedure. The result is shown as PASS if all measurements are approved and FAIL if at least one measurement has failed. Result of test: All measurements were recorded and approved according to the test criteria.'
    ans = input((list_text))
    ans = int(ans)
    
    switcher= {
        1: function_test,
        2: cycle_test,
        3: post_function,
        4: burnin,
        5: vibration,
        6: pressure

    }
    s_text = switcher.get(ans, 'invalid type')
    return s_text


def create_attachment_summary(df):
    """
    for files in glob.glob("* Summary.csv"):
        df = pd.read_csv(files)
    df_summary = df[df.columns[1:]].copy(deep=True)"""
    df_summary = df
    sn = input('Enter EUT serial number: ')
    df_name =  sn
    text =  type_of_test()  
    
    #print(df_summary)

    with pd.option_context('display.precision', 5):
                    df_html = (df_summary.style
                               .applymap(highligth_pass)
                               .set_caption(text)
                               .set_table_attributes('class="pure-table"')
                               .hide_index())
    #df_name=(files[:-7])
    

    pdf_gen(df_name, df_html)


def generate_attachment(df):
    #create_attachment_ut()
    #create_attachment_temp()
    create_attachment_summary(df)


def pdf_gen(df_name, df_html):

    #header = df_name[:-5]
    header = df_name
    css = 'pdf.css'
    options = {
        'page-size': 'A4',
        'orientation': 'Landscape',
        'user-style-sheet': css,
        'header-center': header
    }
    
    pdfkit.from_string(df_html.render(), header+'.pdf', options=options, css=css)


def create_pdf():
    tmp =[]
    for files in glob.glob("*.html"):
        tmp.append(files)
    pool = Pool(processes=18)
    pool.map(pdf_gen, tmp)


def zip_all():
    zip_files = []
    zip_json()
    zip_csv()
    


def cleanup_html():
    for files in glob.glob("*.html"):
        try:
            os.remove(files)
        except:
            pass

def read_rev():
    
    rev = eval(open('rev.txt').read())
    test_object = rev['type']
    return test_object

if __name__ == "__main__":
    change_folder()
    test_object = read_rev()
    a = find_databases()
    df = pd.DataFrame()

    tests = []
    for i in range((len(a))):
        tests.append(prosess_data(a[i], test_object))
        output = tests[i].check_database()
        #tests[i].export_databases()
        df = df.append([output], ignore_index=True)
        #print(df)

    summary_db(df)
    #export_db_summary()

    generate_attachment(df)
    #create_pdf()
    #cleanup_html()

    zip_all()
