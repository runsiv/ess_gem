import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import sys
import glob
import pathlib
import concurrent.futures
from zipfile import ZipFile
import os
import csv
import pdfkit
import multiprocessing.dummy as mp
from multiprocessing import Process
from threading import Thread
from multiprocessing import Pool
import seaborn as sns
#css = 'pdf.css'
from tkinter import filedialog
from tkinter import *
root = Tk()
root.withdraw()
folder_selected = filedialog.askdirectory()


def change_folder():
    os.chdir(folder_selected)


def find_databases():
    listofdb = []
    for files in glob.glob("*.db"):
        listofdb.append(files)
        # print(files)
    return listofdb

"""
Class for reading the databases and export it to csv
"""
class prosess_data:
    def __init__(self, db, test_object):
        self.cur = None
        self.conn = None
        self.numberofmeas = None
        self.number_of_us_passed = None
        self.number_of_us_failed = None
        self.number_of_temp_failed = None
        self.number_of_temp_passed = None
        self.db = db
        self.sn = db[:-3]
        self.passed = None
        self.summary = None
        self.csv = None
        self.df = pd.DataFrame()
        self.test_object = test_object
        self.temp_std = None
        self.us_std = None 
        self.us_mean = None
        self.us_min = None 
        self.us_max = None 
        self.temp_mean = None
        self.temp_min = None 
        self.temp_max = None

    def connect_to_db(self):
        # print(self.db)
        try:
            self.conn = sqlite3.connect(self.db)
            self.cur = self.conn.cursor()
        except Error as e:
            print(e)
        print(self.sn)

    def export_db_us(self):
        f = open("S2M64 - " + self.sn+(' Ultrasound Test Data.csv'), 'w')
        self.cur.execute('select * from Ultrasound')
        rows = self.cur.fetchall()
        column_names = [i[0] for i in self.cur.description]
        files = csv.writer(f, lineterminator='\n')
        files.writerow(column_names)
        files.writerows(rows)

        f.close()

    # Summary of all temperature channels, calculates min, max and mean of all data
    def temperature_std(self):
        df = pd.read_sql_query("Select * from Temperature", self.conn)
        df_all = pd.DataFrame()
        for i in range(1,5):
            string = "ch{}".format(i)
            df_all = df_all.append(df[string])
        tmp_t = df_all.transpose()
        tmp_t = np.std(tmp_t)
        self.temp_std =np.std(tmp_t)

        temp_mean = tmp_t.mean()
        self.temp_mean = temp_mean.mean()

        temp_min = tmp_t.min()
        self.temp_min = temp_min.min()

        temp_max = tmp_t.max()
        self.temp_max = temp_max.max()

        

    # Summary of all ultrasound channels, calculates min, max and mean of all data
    def ultrasound_std(self):
        df = pd.read_sql_query("Select * from Ultrasound", self.conn)
        df_all = pd.DataFrame()
        for i in range(1,35):
            string = "ch{}_result".format(i)
            df_all = df_all.append(df[string])
        tmp_us_t = df_all.transpose()
        tmp_us_std = np.std(tmp_us_t)
        us_mean = tmp_us_t.mean()
        self.us_mean = us_mean.mean()
        self.us_std =np.std(tmp_us_std)

        us_min = tmp_us_t.min()
        self.us_min = us_min.min()

        us_max = tmp_us_t.max()
        self.us_max = us_max.max()

    def export_db_temp(self):
        f = open("S2M64 - " +  self.sn+(' Temperature Test Data.csv'), 'w')
        self.cur.execute('select * from Temperature')
        rows = self.cur.fetchall()
        column_names = [i[0] for i in self.cur.description]
        files = csv.writer(f, lineterminator='\n')
        files.writerow(column_names)
        files.writerows(rows)

        f.close()
    """
    def export_db_summary(self):
        f = open('summary.csv', 'w')
        self.cur.execute('select * from Summary')
        rows = self.cur.fetchall()
        column_names = [i[0] for i in self.cur.description]
        files = csv.writer(f, lineterminator='\n')
        files.writerow(column_names)
        files.writerows(rows)

        f.close()
    """

    def number_of_measurements(self):
        self.cur.execute('''SELECT COUNT(*) FROM Temperature''')
        count = self.cur.fetchall()
        self.numberofmeas = int(count[0][0])


    def look_for_fail_in_temperature(self):
        temp_channel_status = []
        ch_list = []
        counter = 0
        if self.test_object == 'transducer_holder':
            temp_len=5

        if self.test_object =='pcea':
            temp_len=17

        if self.test_object =="int_frame":
            temp_len=17

        for i in range(1, temp_len):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Temperature where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            if state[0] == ((0),):
                temp_channel_status.append('Pass')
            else:
                temp_channel_status.append(state[0])
                counter = counter + 1

        self.number_of_temp_passed = temp_len - counter
        self.number_of_temp_failed = counter
        # return Temperature_status

    def look_for_fail_in_ultrasound(self):
        us_channel_status = []
        ch_list = []
        counter = 0
        if self.test_object == 'transducer_holder':
            ut_len=35
        if self.test_object =='pcea':
            ut_len=65
        for i in range(1, ut_len):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Ultrasound where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            # print(state)
            # print(type(state))
            if state[0] == ((0),):
                us_channel_status.append('Pass')
            else:
                us_channel_status.append(state[0])
                counter = counter + 1
        self.number_of_us_failed = counter
        self.number_of_us_passed = ut_len - counter




    def create_summary(self):
        if self.number_of_us_passed == 64 or self.number_of_us_passed == 35 and self.number_of_temp_passed == 16 or self.number_of_temp_passed == 5 :
            self.passed = 'Pass'
        else:
            self.passed = 'Fail'
        self.summary = {
            "S2M64_SN ": self.sn,
            "Status ": self.passed,
            "Meas_Cycles_Done ": self.numberofmeas,
            "UT_Ch_Passed ": self.number_of_us_passed,
            "UT_Ch_Failed ": self.number_of_us_failed,
            "T_Ch_Passed ": self.number_of_temp_passed,
            "T_Ch_Failed ": self.number_of_temp_failed,
            "T_Min_[tempC]" : self.temp_min,
            "T_Max_[tempC]" : self.temp_max,
            "T_Avg_[tempC]" : self.temp_mean,
            #"STD_Temp_[tempC]": self.temp_std, 
            "UT_Min_[Energy]" : self.us_min, 
            "UT_Max_[Energy]" : self.us_max,
            "UT_Avg_[Energy]": self.us_mean}
        #print(self.summary)
        return self.summary

    def check_database(self):
        self.connect_to_db()
        self.number_of_measurements()
        self.look_for_fail_in_ultrasound()
        self.look_for_fail_in_temperature()
        self.temperature_std()
        self.ultrasound_std()
        self.create_summary()
        return self.summary

    def export_databases(self):
        self.export_db_us()
        self.export_db_temp()
        # self.export_db_summary()
    """
    def generate_attachment(self):
        self.create_attachment_ut()
        self.create_attachment_temp()
        self.create_attachment_summary()
    """


def summary_db(df):
    try:
        conn = sqlite3.connect('summary.db')
    except Error as e:
        print(e)
    try:
        df.to_sql('Summary', con=conn, if_exists='append')
    except Error as e:
        print(e)


def export_db_summary():
    try:
        conn = sqlite3.connect('summary.db')
        cur = conn.cursor()
    except Error as e:
        print(e)
    sn = input('Enter EUT serial number: ')
    filname = test_object.upper() + ' ' +sn + " Summary.csv"
    f = open(filname, 'w')
    cur.execute('select * from Summary')
    rows = cur.fetchall()
    column_names = [i[0] for i in cur.description]
    files = csv.writer(f, lineterminator='\n')
    files.writerow(column_names)
    files.writerows(rows)

    f.close()

# compresses all .csv files found in the directory


def zip_csv():
    raw_files = []
    for files in glob.glob("*.csv"):
        raw_files.append(files)

    with ZipFile('csv_files.zip', 'w') as zip:
        for file in raw_files:
            zip.write(file)
    for file in raw_files:
        try:
            os.remove(file)
        except:
            pass


def zip_json():
    raw_files = []
    for files in glob.glob("*.json"):
        raw_files.append(files)

    with ZipFile('json_files.zip', 'w') as zip:
        for file in raw_files:
            zip.write(file)
    for file in raw_files:
        try:
            os.remove(file)
        except:
            pass


def create_attachment_ut():
    csv_files = []
    for files in glob.glob("*.csv"):
        csv_files.append(files)
        create_df(files)

"""
def create_attachment_temp():
    csv_files = []
    for files in glob.glob("*.csv"):
        csv_files.append(files)
        create_df(files)
"""
def highligth_pass(data):
    '''
    hightlight pass as green
    '''
    color = 'black'
    if data =='Pass':
        color ='green'
    if data =='Fail':
        color ='red'


    return 'color: %s' % color

def create_df(files):
    df = pd.read_csv(files)
    df_count = len(df.columns)
    df_row_count = df.shape[0]
    #row_divider_counter = int(df_row_count/29)
    #print(df_count)
    if df_count == 13:
        loop_length = 1
    else:
        loop_length = int((df_count-3)/32)
    df_start = df[df.columns[1:3]].copy(deep=True)

    """
    The exported csv files are splited into multiple dataframes which again is exported to html
    This is done in order to remove lines crossing into oneanother when the html table is spanning multiple lines
    df_start contains the timestamp and serial number while df_subset
    df_tmp contains the n-frist 32 colomns
    df_subset containts the n-first 28 rows of df_tmp
    df_sub_start contains the n-first 28 rows of df_start

    """
    for i in range(loop_length):

        df_tmp = df[df.columns[3+(i*32):35+(i*32)]].copy(deep=True)
        chunksize = 23
        start = 0
        counter = 0

        for start in range(0, df_row_count, chunksize):
            counter = 1 + counter
            df_sub_start = df_start.iloc[start:start + chunksize]
            df_subset = df_tmp.iloc[start:start + chunksize]

            df_csv = df_sub_start.join(df_subset)
            df_csv.style.hide_index()

            if i ==0:
                df_str = files[:-14] + ' Measurements, Channel ' + str(1) + " to " + str(
                    16+(i*16)) + ", page " + str(counter) + " of " + str((int((df_row_count/2)+1)))
            else:
                df_str = files[:-14] + ' Measurements, Channel ' + str(1+(i*16)) + " to " + str(
                    16+(i*16)) + ", page " + str(counter) + " of " + str((int((df_row_count/2)+1)))
            if 'Ultrasound' in df_str:
                caption= df_str[19:] +' [Energy]'
                with pd.option_context('display.precision', 5):
                    df_html = (df_csv.style
                            .applymap(highligth_pass)
                            .set_caption(caption)
                            .set_table_attributes('class="pure-table"'))
            if 'Temperature' in df_str:
                caption= df_str[19:]+' [°C]'
                with pd.option_context('display.precision', 1):
                    df_html = (df_csv.style
                            .applymap(highligth_pass)
                            .set_caption(caption)
                            .set_table_attributes('class="pure-table"'))
            """
            with pd.option_context('display.precision', 2):
                df_html = (df_csv.style
                    .applymap(highligth_pass)
                    .set_caption(caption)
                    .set_table_attributes('class="pure-table"'))
                    """
            """
            df_csv.style.applymap(highligth_pass)
            df_csv.style.format("{:.1f}")
            if 'Ultrasound' in df_str:
                print('Ultrasound')
                df_csv.style.set_caption('[Energy] ')
            if 'Temperature' in df_str:
                df_csv.style.set_caption('[Celsius] ')
            #cm = sns.light_palette("seagreen", as_cmap=True)
            df_html = df_csv.style.applymap(highligth_pass)
            #df_html.style.background_gradient(cmap=cm)
            """
            pdf_gen(df_str, df_html)

            #df_csv.to_html(df_str + '.html')


def create_attachment_summary(df):
    """
    for files in glob.glob("* Summary.csv"):
        df = pd.read_csv(files)
    df_summary = df[df.columns[1:]].copy(deep=True)"""
    df_summary = df
    sn = input('Enter EUT serial number: ')
    df_name = "PCEA SN " + sn
    
    #print(df_summary)

    with pd.option_context('display.precision', 5):
                    df_html = (df_summary.style
                               .applymap(highligth_pass)
                               .set_caption('Test Summary Table')
                               
                               .set_table_attributes('class="pure-table"'))
    #df_name=(files[:-7])
    

    pdf_gen(df_name, df_html)


def generate_attachment(df):
    #create_attachment_ut()
    #create_attachment_temp()
    create_attachment_summary(df)


def pdf_gen(df_name, df_html):

    #header = df_name[:-5]
    header = df_name
    css = 'pdf.css'
    options = {
        'page-size': 'A4',
        'orientation': 'Landscape',
        'user-style-sheet': css,
        'header-center': header
    }
    pdfkit.from_string(df_html.render(), header+'.pdf', options=options, css=css)


def create_pdf():
    tmp =[]
    for files in glob.glob("*.html"):
        tmp.append(files)
    pool = Pool(processes=18)
    pool.map(pdf_gen, tmp)


def zip_all():
    zip_files = []
    zip_json()
    zip_csv()
    """
    for files in glob.glob("*.zip"):
        zip_files.append(files)

    with ZipFile('test_files.zip', 'w') as zip:
        for file in zip_files:
            zip.write(file)

    os.remove('json_files.zip')
    os.remove('csv_files.zip')
    """


def cleanup_html():
    for files in glob.glob("*.html"):
        try:
            os.remove(files)
        except:
            pass

def read_rev():
    rev = eval(open('rev.txt').read())
    test_object = rev['type']
    return test_object

if __name__ == "__main__":
    change_folder()
    test_object = read_rev()
    a = find_databases()
    df = pd.DataFrame()

    tests = []
    for i in range((len(a))):
        tests.append(prosess_data(a[i], test_object))
        output = tests[i].check_database()
        #tests[i].export_databases()
        df = df.append([output], ignore_index=True)
        #print(df)

    summary_db(df)
    #export_db_summary()

    generate_attachment(df)
    #create_pdf()
    #cleanup_html()

    zip_all()
