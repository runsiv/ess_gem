import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
#import serial
import time
#import threading
#from threading import Thread
#import concurrent.futures
#import um_functions.um_func as um_func
import logging
import signal
import argparse
import json
import os
import time
import csv
import glob
import shutil
import tarfile
from tkinter import filedialog
from tkinter import *
root = Tk()
root.withdraw()
folder_selected = filedialog.askdirectory()
#test_folder = '\\test_data'
os.chdir(folder_selected)

def json_file_finder():
    file_list = glob.glob("*.json")
    measurement_files = []
    for files in file_list:
        measurement_files.append(files)
    return measurement_files

def create_db():
        try:
            db_name = str(input('Enter System_A or System_B: '))
            db = db_name+'.db'
            conn = sqlite3.connect(db)
        except Error as e:
            print(e)
            logging.error(e)
        return db

class database_class:
    def __init__(self, db, tmp_ut, tmp_uts, tmp_t, tmp_ts):
        self.db = db
        self.co = None
        self.db_name = None
        self.cur = None
        self.number_of_measurements_t = None
        self.number_of_measurements_ut = None
        self.number_of_temp_passed = None
        self.number_of_temp_failed = None
        self.tmp_ut = tmp_ut
        self.tmp_uts = tmp_uts
        self.tmp_t =tmp_t
        self.tmp_ts = tmp_ts


    

    def connect_to_db(self):
        # print(self.db)
        try:
            self.co = sqlite3.connect(self.db)
            self.cur = self.co.cursor()
        except Error as e:
            print(e)
    
    def send_to_table(self):
        try:
            tmp_t.to_sql('Temperature', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)
        try:
            tmp_ts.to_sql('Temperature Status', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)
        try:
            tmp_ut.to_sql('Ultrasound', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)
            #print("ULTRASOUND")
        try:
            tmp_uts.to_sql('Ultrasound Status', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)
            #print("ULTRASOUND")
        



    def number_of_measurements(self):
        self.cur.execute('''SELECT COUNT(*) FROM Temperature''')
        count_t = self.cur.fetchall()
        self.number_of_measurements_t = int(count_t[0][0])




def tar_unpacker(file):
    tar =tarfile.open(file)
    tar.extractall()

def map_remover():
    shutil.rmtree('tmp')

def tar_finder():
    tar_list = glob.glob("*.tar.xz")
    return tar_list



class file_handler:
    def __init__(self, json_file):
        #self.sn = None
        self.trace = None
        self.calc_trace = None
        self.ut_id = None
        self.json_file = json_file
        self.ts = None
        self.status_ut = None
        self.status_t = None
        self.meas_dict = None
        self.temp_id = None
        self.temp = None 
        self.json_dict = None
        self.ut_dict = None
        self.t_dict = None
        


        


    def load_json(self):
        
        with open(self.json_file) as myfile:
            data = myfile.read()
        obj = json.loads(data)
        #plt.plot(obj['measurements'][0]['trace'])
        self.meas_dict = obj['measurements'][0]
        self.ts = obj['daytime']
        self.temp_id = self.meas_dict['temp_id']
        self.temp = self.meas_dict['temp_pipe']
        self.ut_id = self.meas_dict['unique_id']
        



    def ut_checker(self):
        self.trace = self.meas_dict['trace']
        self.calc_trace = np.sum(np.abs(self.trace[0:99]))

        if self.calc_trace > 10000:
            self.status_ut = 'Pass'
        else:
            self.status_ut = 'Fail'
    
    def temp_checker(self):
        if self.temp < 5 and self.temp > -5:
            self.status_t = "Pass"
        else:
            self.status_t = "Fail"
        


    def create_dict(self):
        self.ut_dict = {
            'Timestamp': self.ts,
            'UT_ID': self.ut_id,
            'Calc_trace': self.calc_trace,
            'Status': self.status_ut,
            #'T_ID': self.temp_id,
            #'Status': self.status_t

        }
        """
        self.ut_dict = {
            'Timestamp': self.ts,
            'UT_ID': self.ut_id,
            'Calc_trace': self.calc_trace,
            'Status': self.status_ut
            
        }
        """
        self.t_dict = {
            'Timestamp': self.ts,
            'T_ID': self.temp_id,
            'T_temp': self.temp,
            'Status': self.status_t
        }
        

    def prosses_data(self):
        self.load_json()
        self.ut_checker()
        self.temp_checker()
        self.create_dict()
        return self.ut_dict, self.t_dict

class merger:
    def __init__(self, df_ut,df_t):
        #self.sn = None
        self.df_ut_old = df_ut
        self.df_t_old = df_t

        self.df_ut_new = pd.DataFrame()
        self.df_t_new = pd.DataFrame()
        self.ut_list = None
        self.t_list = None 
        self.df_ut = pd.DataFrame()
        self.df_uts = pd.DataFrame()
        self.df_t = pd.DataFrame()
        self.df_ts = pd.DataFrame() 

    # Generate id list for UT and Temperature
    def make_list(self):
        init_str_ut = "UT"
        init_str_uts = "UT_status"
        init_str_t = "T"
        init_str_ts = "T_status"
        ut = []
        t = []
        uts = []
        ts = []
        ch = []
        for i in range(1,33):
            tmp_ut = init_str_ut + "{}".format(i)
            tmp_t = init_str_t + "{}".format(i)
            tmp_uts = init_str_uts + "{}".format(i)
            tmp_ts = init_str_t + "{}".format(i)
            ut.append(tmp_ut)
            t.append(tmp_t)
            uts.append(tmp_ut)
            ts.append(tmp_ts)
        self.ut_list = ut
        self.t_list = t
        self.uts_list = uts
        self.ts_list = ts

    def build_UT_table(self):
        ts = self.df_ut_old['Timestamp'][0]
        tmp = zip(self.ut_list, self.df_ut_old['Calc_trace'])
        ut_tmp_dict = dict(tmp)
        ut_tmp_dict['Timestamp'] = ts
        self.df_ut = self.df_ut.append(ut_tmp_dict, ignore_index=True)
        
    
    def build_UT_status_table(self):
        ts = self.df_ut_old['Timestamp'][0]
        tmp = zip(self.ut_list, self.df_ut_old['Status'])
        uts_tmp_dict = dict(tmp)
        uts_tmp_dict['Timestamp'] = ts
        self.df_uts = self.df_uts.append(uts_tmp_dict, ignore_index=True)

    def build_T_table(self):
        ts = self.df_t_old['Timestamp'][0]
        tmp = zip(self.t_list, self.df_t_old['T_temp'])
        t_tmp_dict = dict(tmp)
        t_tmp_dict['Timestamp'] = ts
        self.df_t = self.df_t.append(t_tmp_dict, ignore_index=True)
    
    def build_t_status_table(self):
        ts = self.df_t_old['Timestamp'][0]
        tmp = zip(self.ts_list, self.df_t_old['Status'])
        ts_tmp_dict = dict(tmp)
        ts_tmp_dict['Timestamp'] = ts
        self.df_ts = self.df_ts.append(ts_tmp_dict, ignore_index=True)

    def prosess(self):
        self.make_list()
        self.build_UT_table()
        self.build_UT_status_table()
        self.build_T_table()
        self.build_t_status_table()
        return self.df_ut, self.df_uts, self.df_t, self.df_ts
    

def cleanup_json():
    for files in glob.glob("*.json"):
        try:
            os.remove(files)
        except:
            pass

if __name__ == "__main__":
    #ut_dicts = {}
    #t_dicts = {}
    start = time.time()
    df_ut = pd.DataFrame()
    df_t = pd.DataFrame()
    merg = merger(df_ut, df_t)
    db_name = create_db()
    #db = database_class()

    tar_list = tar_finder()
    
    for files in tar_list:
        tar_unpacker(files)
        json_files = json_file_finder()
        df_ut = pd.DataFrame()
        df_t = pd.DataFrame()
        for f in json_files:
           fh = file_handler(f)
           ut_meas_dict, t_meas_dict= fh.prosses_data()
           df_ut = df_ut.append([ut_meas_dict], ignore_index=True)
           df_t = df_t.append([t_meas_dict], ignore_index=True)
        merg = merger(df_ut, df_t)
        tmp_ut, tmp_uts, tmp_t, tmp_ts = merg.prosess()
        db = database_class(db_name, tmp_ut, tmp_uts, tmp_t, tmp_ts)
        db.connect_to_db()
        db.send_to_table()
        cleanup_json()
    end = time.time() 
    print("Execution time: ",end - start)

        
        
    

           
    













