import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import sys
import glob
import pathlib
import concurrent.futures
import os
import csv
import multiprocessing.dummy as mp
from multiprocessing import Process
from threading import Thread
from multiprocessing import Pool
#css = 'pdf.css'


def change_folder():
    os.chdir('test_data')


def find_databases():
    listofdb = []
    for files in glob.glob("*.db"):
        listofdb.append(files)
        # print(files)
    return listofdb

"""
Class for reading the databases and export it to csv
"""
class prosess_data:
    def __init__(self, db, test_object):
        self.cur = None
        self.numberofmeas = None
        self.number_of_us_passed = None
        self.number_of_us_failed = None
        self.number_of_temp_failed = None
        self.number_of_temp_passed = None
        self.db = db
        self.sn = db[:-3]
        self.passed = None
        self.summary = None
        self.csv = None
        self.df = pd.DataFrame()
        self.test_object = test_object

    def connect_to_db(self):
        # print(self.db)
        try:
            conn = sqlite3.connect(self.db)
            self.cur = conn.cursor()
        except Error as e:
            print(e)
        print(self.sn)



    def number_of_measurements(self):
        self.cur.execute('''SELECT COUNT(*) FROM Temperature''')
        count = self.cur.fetchall()
        self.numberofmeas = int(count[0][0])


    def look_for_fail_in_temperature(self):
        temp_channel_status = []
        ch_list = []
        counter = 0
        if self.test_object == 'transducer_holder':
            temp_len=5
        if self.test_object =='pcea':
            temp_len=16
        for i in range(1, temp_len+1):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Temperature where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            if state[0] == ((0),):
                temp_channel_status.append('Pass')
            else:
                temp_channel_status.append(state[0])
                counter = counter + 1

        self.number_of_temp_passed = 16 - counter
        self.number_of_temp_failed = counter
        # return Temperature_status

    def look_for_fail_in_ultrasound(self):
        us_channel_status = []
        ch_list = []
        counter = 0
        if self.test_object == 'transducer_holder':
            ut_len=36
        if self.test_object =='pcea':
            ut_len=65
        for i in range(1, ut_len):
            ch = 'ch{}_state'.format(i)
            ch_list.append(ch)
            query = "SELECT COUNT({}) FROM Ultrasound where {} = 'Fail'".format(
                ch, ch)
            self.cur.execute(query)
            state = (self.cur.fetchall())
            # print(state)
            # print(type(state))
            if state[0] == ((0),):
                us_channel_status.append('Pass')
            else:
                us_channel_status.append(state[0])
                counter = counter + 1
        self.number_of_us_failed = counter
        self.number_of_us_passed = 64 - counter


    def create_summary(self):
        if self.number_of_us_passed == 64 and self.number_of_temp_passed == 16:
            self.passed = 'Passed'
        else:
            self.passed = 'Failed'
        self.summary = {
            "SN": self.sn,
            "Passed": self.passed,
            "Number_of_Measurements": self.numberofmeas,
            "Ultrasound_Channels_Passed": self.number_of_us_passed,
            "Ultrasound_Channels_Failed": self.number_of_us_failed,
            "Temperature_Channels_Passed": self.number_of_temp_passed,
            "Temperature_Channels_Failed": self.number_of_temp_failed}
        return self.summary

    def check_database(self):
        self.connect_to_db()
        self.number_of_measurements()
        self.look_for_fail_in_ultrasound()
        self.look_for_fail_in_temperature()
        self.create_summary()
        return self.summary




def read_rev():
    rev = eval(open('rev.txt').read())
    test_object = rev['type']
    return test_object

if __name__ == "__main__":
    change_folder()
    test_object = read_rev()
    a = find_databases()
    df = pd.DataFrame()

    tests = []
    for i in range((len(a))):
        tests.append(prosess_data(a[i], test_object))
        output = tests[i].check_database()
        #tests[i].export_databases()
        df = df.append([output], ignore_index=True)

    print(df)
    print(df['Number_of_Measurements'])
    print(df['Ultrasound_Channels_Passed'])
    print(df['Temperature_Channels_Passed'])



