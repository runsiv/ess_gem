---
###Title: th_pressure.py
---

## How to use it:

Use 2 RS 485 lines with 4 S2M64 on the first com port and 3 on the second:
1.	Connect the transducers to the S2M64 in the following order:
-	The 15-transducer connector to P6001
-	The 14-transducer connector to P7001
-	The 6-transducer connector to P8001
-	The temperature sensors to P1001
2. Chain the S2M64 togheter, take note of the serial number of the transducer holder and the S2M64
3. Run the script th_pressure.py with the following variables
"python th_pressure.py comXX comYY 2 4"
4.follow the instruction on the terminal
5. when done with the test, close the terminal


