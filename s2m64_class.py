import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import serial
import time
import threading
from threading import Thread
import concurrent.futures
import um_functions.um_func as um_func
import logging
import signal
import argparse
import json
import os
import time
um = um_func.SwarmActions()

"""
Class for handling the S2M64, preforming the measurement and send it to the database
This class is used in two different types of applications PCEA and transducer_holder
"""
class S2m64:
    def __init__(self, ser, um, add, mode, ref, client):
        self.serial_port = ser
        self.chlist_ut = None
        self.chlist_ut_raw = None
        self.chlist_t = None
        self.co = None
        self.um = um
        self.add = add
        self.df_ut = pd.DataFrame()
        self.df_t = pd.DataFrame()
        self.measure_ut_raw = None
        self.sn = um.read_serial(ser, self.add).decode('ascii')
        self.ts = None
        self.unixts = None
        self.counter = 0
        self.mode = mode
        self.ref = ref
        self.client = client


    # connect to the database with the same name as the serial number
    def create_db(self):
        try:
            db = self.sn+'.db'
            self.co = sqlite3.connect(db)
        except Error as e:
            print(e)
            logging.error(e)
        return self.co
    
    # extra check for test blocks
    def checker(self, meas, meas_old):
        
        for i in range(15):
            if meas[i] > 10000: #and meas != meas_old:
                status = True
            else:
                status = False
        return status 

    # channel list for UT and Temperature

    def create_chlist(self):
        # create ut channel list
        self.chlist_ut = []
        self.chlist_ut_raw = []
        self.chlist_ut.append('Serial_Number')
        self.chlist_ut.append('Timestamp')
        self.chlist_ut.append('Address')
        self.chlist_ut_raw.append('Serial_Number')
        self.chlist_ut_raw.append('Timestamp')
        self.chlist_ut_raw.append('Address')
        if self.mode == '0':
            for i in range(1, 65):
                #self.chlist_ut.append('ch{}'.format(i))
                self.chlist_ut.append('ch{}_result'.format(i))
                self.chlist_ut.append('ch{}_state'.format(i))
                self.chlist_ut_raw.append('ch{}'.format(i))
            self.chlist_t = []
            # create temp channel list
            self.chlist_t.append('Serial_Number')
            self.chlist_t.append('Timestamp')
            for i in range(1, 17):
                self.chlist_t.append('ch{}'.format(i))
                self.chlist_t.append('ch{}_state'.format(i))

        if self.mode == '1' or self.mode == '2' or self.mode == '3' or self.mode == '4':
            # endring fra 37
            for i in range(1, 37):
                #self.chlist_ut.append('ch{}'.format(i))
                self.chlist_ut.append('ch{}_result'.format(i))
                self.chlist_ut.append('ch{}_state'.format(i))
                self.chlist_ut_raw.append('ch{}'.format(i))
            self.chlist_t = []
            # create temp channel list
            self.chlist_t.append('Serial_Number')
            self.chlist_t.append('Timestamp')
            for i in range(1, 6):
                self.chlist_t.append('ch{}'.format(i))
                self.chlist_t.append('ch{}_state'.format(i))
        
        if self.mode == '5':
            for i in range(1, 17):
                #self.chlist_ut.append('ch{}'.format(i))
                self.chlist_ut.append('ch{}_result'.format(i))
                self.chlist_ut.append('ch{}_state'.format(i))
                self.chlist_ut_raw.append('ch{}'.format(i))
            self.chlist_t = []
            # create temp channel list
            self.chlist_t.append('Serial_Number')
            self.chlist_t.append('Timestamp')
            for i in range(0, 3):
                self.chlist_t.append('ch{}'.format(i))
                self.chlist_t.append('ch{}_state'.format(i))

        if self.mode == '6':
            for i in range(1, 16):
                #self.chlist_ut.append('ch{}'.format(i))
                self.chlist_ut.append('ch{}_result'.format(i))
                self.chlist_ut.append('ch{}_state'.format(i))
                self.chlist_ut_raw.append('ch{}'.format(i))
            self.chlist_t = []
            # create temp channel list
            self.chlist_t.append('Serial_Number')
            self.chlist_t.append('Timestamp')
            for i in range(1, 2):
                self.chlist_t.append('ch{}'.format(i))
                self.chlist_t.append('ch{}_state'.format(i))
        return self.chlist_ut, self.chlist_t

    """
    Measures ultrasound, creates a sum for the 100 first samples and chekc if it above 10k,
    the results is sent to a dataframe (df)
    The measure UT supports "2" different modes
    Mode '0' = PCEA, used 64 UT channels
    All else modes are for the transducer holder and suppoerts 35 channels using mux P601, P701 and P801
    """

    def meas_ut_inner(self, measurements_ut, start, stop):
        for i in range(start, stop):
                meas_old = []
                try:
                    
                    meas = um.measure_us_custom(
                        self.serial_port, self.add, i, 0, 400, 1024, 0, 99, 200, 0, 2, 5)
                    if self.mode == '3' or self.mode =='4' or self.mode =='5':
                      meas_calc = (np.sum(np.abs(meas)))/5  
                    else:
                        meas_calc = (np.sum(np.abs(meas)))/20
                    
                    status = self.checker(meas, meas_old)
                    """
                    if self.mode == '3' or self.mode =='5' or self.mode =='4' and meas_calc != meas_old:
                        status = True
                    """
                    if self.mode == '3' or self.mode =='5' or self.mode =='4' or self.mode=='6':
                        status=True
                    self.measurements_ut_raw[self.chlist_ut_raw[i]] = meas
                    #meas_old = meas_calc

                except Exception as e:
                    print(e)
                    logging.error(e)
                    error_msg = (
                        'error reading ut on add {} channel {}'.format(self.add, i))
                    print(error_msg)
                    meas = 1
                    self.measurements_ut_raw[self.chlist_ut_raw[i]] = error_msg


                measurements_ut.append(meas_calc)
                if meas_calc > 10000 and status ==True:
                    measurements_ut.append('Pass')
                else:
                    measurements_ut.append('Fail')
                    logging.error('UT Fail on address {} channel {}'.format(self.add, i))
                    print('UT fail on address {} channel {}'.format(self.add, i))
        return measurements_ut
    
    def measure_ut(self):
        measurements_ut = []
        self.measurements_ut_raw = dict()

        self.ts = datetime.datetime.now().strftime("%Y-%m-%d %H:%M")
        self.unixts = int(time.time())
        self.measurements_ut_raw['timestamp'] = self.ts
        measurements_ut.append(self.sn)
        measurements_ut.append(self.ts)
        measurements_ut.append(self.add)
        ### endring
        #self.measurements_ut_raw(self.sn)
        #self.measurements_ut_raw(self.ts)
        #self.measurements_ut_raw(self.add)
        
        # PCEA Mode
        # UT channels are 1-64
        if self.mode == '0':
            measurements_ut = self.meas_ut_inner(measurements_ut, 1, 65)

        # Mode 2,3 and 4 is for the transducer holder
        # UT channels are 1-15, 17-31, 33-39
        if self.mode == '2' or self.mode == '3' or self.mode == '4':
            # Loops over contact P701
            raw_counter = 0
            measurements_ut = self.meas_ut_inner(measurements_ut, 1, 16)

            # Loops over P801
            measurements_ut = self.meas_ut_inner(measurements_ut, 17, 31)

            # Loops over contact P901
            measurements_ut = self.meas_ut_inner(measurements_ut, 33, 39)
        
        if self.mode == '5':
            measurements_ut = self.meas_ut_inner(measurements_ut, 1, 17)

        if self.mode == '6':
            measurements_ut = self.meas_ut_inner(measurements_ut, 1, 16)
            


        tmp = zip(self.chlist_ut, measurements_ut)
        output_ut = dict(tmp)
        self.df_ut = self.df_ut.append([output_ut])



    def create_json(self):
        json_string = self.sn + '_' + str(self.unixts) + '.json'
        with open(json_string, 'a') as json_file:
            json.dump(self.measurements_ut_raw, json_file, indent=2)


    """
    ADAM-4015 is a RTD standalone measurement device, communicates over modbus
    default setup is to use channel 0, address 0 and a PT1000 element
    Is used as a reference temperature source to do a sanitycheck for  the temperature measurements
    """
    def meas_adam(self):
        nom_ad = 32768
        result = None
        while result is None:
            try:
                result = client.read_holding_registers(address=0, count=1, unit=1)
            except:
                pass
        self.ref = 200*((result.getRegister(0)-nom_ad)/nom_ad)




    """  # Measures temperature
    Mode 0 uses test block and have a accuarity of +-5 degrees. This mode is for the PCEA/ internal frame test and uses all 16 channels
    Mode 1 uses external temp ref --> meas_adam and with a set accuracy of +- 2 degrees
    Mode 2 uses external temp ref --> meas_adam and with a set accuracy of +- 5 degrees
    Mode 3 used set temp ref with a accuracy of +- 2 degrees
    Mode 4 used set temp ref with a accuracy of +- 5 degrees

    """

    def is_float(self, meas):
        try:
            float(meas)
            return True
        except ValueError as e:
            logging.error(e)
            return False

    
    def measure_t(self):
        measurements_t = []
        measurements_t.append(self.sn)
        measurements_t.append(self.ts)
        if self.mode == '0':
            # for PCEA with test blocks
            for i in range(1, 17):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas,3)
                    else:
                        meas = meas
                        print('Error reading temp on add {} channel {}'.format(self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(self.add, i))
                    meas = -5000
                measurements_t.append(meas)
                if meas < 5 and meas > -5:
                    measurements_t.append('Pass')
                else:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))

        # Mode 1 uses external temp ref --> meas_adam and with a set accuracy of +- 2 degrees
        if self.mode == '1':
            self.meas_adam()
            for i in range(1, 6):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas, 3)
                    else:
                        meas = -5000
                        print('Error reading temp on add {} channel {}'.format(
                            self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(
                        self.add, i))
                    meas = -5000
                measurements_t.append(meas)

                if meas < self.ref+2 and meas > self.ref-2:
                    measurements_t.append('Pass')
                else:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))

        # Mode 2 uses external temp ref - -> meas_adam and with a set accuracy of + - 5 degrees
        if self.mode == '2':
            self.meas_adam()
            for i in range(1, 6):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas, 3)
                    else:
                        meas = meas
                        print('Error reading temp on add {} channel {}'.format(
                            self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(
                        self.add, i))
                    meas = -5000
                measurements_t.append(meas)

                if meas < self.ref+5 and meas > self.ref-5:
                    measurements_t.append('Pass')
                else:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))

        # Mode 3 used set temp ref with a accuracy of +- 2 degrees
        if self.mode == '3':
            for i in range(1, 6):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas, 3)
                    else:
                        meas = -5000
                        print('Error reading temp on add {} channel {}'.format(
                            self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(
                        self.add, i))
                    meas = -5000
                measurements_t.append(meas)
                if meas < self.ref+5 and meas > self.ref-5:
                    measurements_t.append('Pass')
                else:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))

        # Mode 4 used set temp ref with a accuracy of +- 5 degrees
        if self.mode == '4':
            for i in range(1, 6):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas, 3)
                    else:
                        meas = -5000
                        print('Error reading temp on add {} channel {}'.format(
                            self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(
                        self.add, i))
                    meas = -5000
                measurements_t.append(meas)
                if meas < self.ref+5 and meas > self.ref-5:
                    measurements_t.append('Pass')
                else:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))
        
        if self.mode == '5':
            # for Retrofit
            for i in range(0, 3):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas,3)
                    else:
                        meas = meas
                        logging.warning(meas)
                        print('Error reading temp on add {} channel {}'.format(self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(self.add, i))
                    meas = -5000
                measurements_t.append(meas)
               
                if meas == -5000:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))
                else:
                    measurements_t.append('Pass')
        if self.mode == '6':
            # for Retrofit
            for i in range(1, 2):
                try:
                    meas = um.measure_temperature_ext(
                        self.serial_port, self.add, i)
                    if self.is_float(meas) == True:
                        meas = round(meas,3)
                    else:
                        meas = meas
                        logging.warning(meas)
                        print('Error reading temp on add {} channel {}'.format(self.add, i))
                except Exception as e:
                    print(e)
                    logging.error(e)
                    print('Error reading temp on add {} channel {}'.format(self.add, i))
                    meas = -5000
                measurements_t.append(meas)
               
                if meas == -5000:
                    measurements_t.append('Fail')
                    logging.error('temperature fail on address {} channel {}'.format(self.add, i))
                    print('temperature fail on address {} channel {}'.format(self.add, i))
                else:
                    measurements_t.append('Pass')


        tmp = zip(self.chlist_t, measurements_t)
        output_t = dict(tmp)
        self.df_t = self.df_t.append([output_t])

    def to_csv(self):
        with open(self.sn+'raw.csv', 'a') as f:
            self.df_ut_raw.to_csv(f, header=f.tell()==0)


    # create dataframe table from channelist
    def create_table(self):
        self.df_t = pd.DataFrame(columns=self.chlist_t)
        self.df_t.set_index = self.counter
        self.df_ut = pd.DataFrame(columns=self.chlist_ut)
        self.df_ut.set_index = 'Timestamp'
        self.df_ut_raw = pd.DataFrame(columns=self.chlist_ut_raw)
        self.df_ut.set_index = self.counter

    # pushes the dataframe to the database
    def push_to_db(self):
        try:
            self.df_t.to_sql('Temperature', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)

        try:
            self.df_ut.to_sql('Ultrasound', con=self.co, if_exists='append', index=True)
        except Exception as e:
            print(e)
            logging.error(e)
            #print("ULTRASOUND")
        self.counter = self.counter + 1

    # collection of functions for a measurement
    def meas_cycle(self):
        self.create_db()
        self.create_table()
        self.measure_ut()
        self.measure_t()
        self.push_to_db()
        self.create_json()
        print('Done with ' +self.sn)



    # collection of first time setup
    def setup(self):
        self.create_db()
        self.create_table()
        self.create_chlist()
