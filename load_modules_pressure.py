import time
import os
import glob
import sys
import logging
import traceback
import datetime
from datetime import date
import csv
import serial
import serial.tools.list_ports
import um_functions.um_func as um_func
import matplotlib.pyplot as plt
import numpy as np
import argparse
um = um_func.SwarmActions()
#parser = argparse.ArgumentParser()
#parser.add_argument('Comport', nargs='*')
#args = parser.parse_args()
#print(len(args))

#print("Enter COM PORT")
#ser_port = input()
sdb1 = serial.Serial("COM17")  # open serial port sdb1
sdb1.baudrate = 115200
sdb1.timeout = 1
sdb1.xonxoff = False

sdb2 = serial.Serial("COM20")  # open serial port sdb1
sdb2.baudrate = 115200
sdb2.timeout = 1
sdb2.xonxoff = False


sdb3 = serial.Serial("COM21")  # open serial port sdb1
sdb3.baudrate = 115200
sdb3.timeout = 1
sdb3.xonxoff = False

sdb4 = serial.Serial("COM22")  # open serial port sdb1
sdb4.baudrate = 115200
sdb4.timeout = 1
sdb4.xonxoff = False

def meas_temp(sdb, add):
	for i in range(1,6):
		t = um.measure_temperature_ext(sdb,add,i)
		print("sensor {}: temp: {}".format(i,t ))

def meas_ut1(sdb, add):
    for i in range(1,16):
        meas = um.measure_us_custom(sdb, add, i, 0, 400, 1024, 0, 99, 200, 0, 2, 5)
        plt.plot(meas)
    plt.show()

def meas_ut2(sdb, add):
    for i in range(1,15):
        meas = um.measure_us_custom(sdb, add, i, 0, 400, 1024, 0, 99, 200, 0, 2, 5)
        plt.plot(meas)
    plt.show()

def meas_ut3(sdb, add):
    for i in range(1,7):
        meas = um.measure_us_custom(sdb, add, i, 0, 400, 1024, 0, 99, 200, 0, 2, 5)
        plt.plot(meas)
    plt.show()

"""
class sdb(): 
    def __init__(self, port):
        self.serial_port = None
        self.port = port
        self.baudrate = 115200
        self.timeout = 1 
        self.xonxonxoff = False
    
    def setup_serial(self):
        self.serial_port = serial.Serial(self.port)
        self.serial_port.baudrate = self.baudrate
        self.serial_port.timeout = self.timeout
        self.serial_port.xonxoff = self.xonxoff
    





 """

