import numpy as np
import pandas as pd
import sqlite3
from sqlite3 import Error
import datetime
import serial
import time
import threading
from threading import Thread
import concurrent.futures
import um_functions.um_func as um_func
from s2m64_class import S2m64
import logging
import signal
import argparse
import json
import os
import time
import pymodbus
from pymodbus.pdu import ModbusRequest
from pymodbus.client.sync import ModbusSerialClient as ModbusClient #initialize a serial RTU client instance
from pymodbus.transaction import ModbusRtuFramer
logging.basicConfig(filename='th_error.txt', level=logging.ERROR,
                    format='%(asctime)s %(message)s', datefmt='%H:%M:%S')

parser = argparse.ArgumentParser(description="Script for ESS testing PCEA")
parser.add_argument('sdb1', help='Com port for SDB1')
parser.add_argument('interval', help='Samling interval in minutes')
parser.add_argument('mode', help=" 0 for test blocks, 1 for +-2 degree with external ref, 2 for +-5 with external ref, 3 for +-2 with user input start temp ")
parser.add_argument('number', help= "Number of S2M64")
#parser.add_argument('sdb2', help='Com port for SDB2')

args = parser.parse_args()
um = um_func.SwarmActions()
path = os.getcwd()
test_folder = '\\test_data'
new_path = path+test_folder
if not os.path.isdir(new_path):
    os.makedirs(new_path)
os.chdir(new_path)






def create_rev_file():
    rev = {
        'rev':'0.1',
        'release date': '15/11/2019',
        'type': 'transducer_holder'
    }
    filename = 'rev.txt'
    if os.path.isfile('./'+filename)==False:
        with open(filename, 'w') as f:
            f.write(str(rev))



def setup_serial_1(port):
    #port = 'COM13'
    ser = serial.Serial(port)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.xonxoff = False
    return ser

# setup serial port 2
# TODO: Enable SDB2 if needed
"""
def setup_serial_2():
    port = 'COM12'
    ser = serial.Serial(port)
    ser.baudrate = 115200
    ser.timeout = 1
    ser.xonxoff = False
    return ser
"""
# class handling threading of serial buss 1, and serial bus 2


class threaderClass(threading.Thread):
    def __init__(self, threadID,  functioncall, sdb):
        threading.Thread.__init__(self, daemon=True)
        #super(threaderClass, self).__init__()
        self.threadID = threadID
        self.functioncall = functioncall
        self.sdb = sdb
        self._return = None
        self._stop_event = threading.Event()

    def run(self):
        self._return = self.functioncall(self.sdb)

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def join(self):
        threading.Thread.join(self)
        return self._return


def create_s2m64_bus1(ser, um, mode, ref, client,nbr):
    

    
    add = []
    for i in range(nbr):
        #add.append(int(input('Address for S2M64 {} for sdb1: '.format(i+1))))
        address = int(input('enter address: '))
        add.append(address)
     
    sdb1 = []
    for i in range(len(add)):
        sdb1.append(S2m64(ser,um,add[i], mode, ref, client))
        sdb1[i].setup()
      
    return sdb1

"""
def create_s2m64_bus2(ser, um, mode, ref, client):
    add = []
    for i in range(3):
        add.append(int(input('Address for S2M64 {} for sdb2: '.format(i+1))))
    sdb2 = []
    for i in range(len(add)):
        sdb2.append(S2m64(ser,um,add[i], mode, ref,client))
        sdb2[i].setup()
    return sdb2
"""

def measure_sdb(sdb):
    for i in range(len(sdb)):
        sdb[i].meas_cycle()


if __name__ == '__main__':
    create_rev_file()
    interval = int(args.interval)
    mode = args.mode
    nbr = int(args.number)
    if mode == '3' or mode =='4':
        ref = float(input('Enter ambient temperature: '))
        client = None
    if mode ==1 or mode ==2:
        ref = 0
        client= ModbusClient(method = "rtu", port="COM11", stopbits = 1, bytesize = 8, parity = 'N', baudrate= 9600)
        connection = client.connect()
    ser_1 = setup_serial_1(args.sdb1)
    #ser_2 = setup_serial_2()


    # TODO: enable sdb2
    sdb1 = create_s2m64_bus1(ser_1, um, mode, ref, client,nbr )
    #sdb2 = create_s2m64_bus2(ser_2, um, mode, ref, client )

    thread_1 = threaderClass(1, measure_sdb, sdb1)
    #thread_2 = threaderClass(1, measure_sdb, sdb2)
    #thread_2.start()
    thread_1.start()
    #thread_2.join()
    thread_1.join()

    print('init run done')

    while(1):

        start = datetime.datetime.now()
        next_interval = start + datetime.timedelta(minutes=interval)

        print("Next measurement", next_interval)
        while next_interval > datetime.datetime.now():
            time.sleep(1)
        print(datetime.datetime.now())
        thread_1 = threaderClass(1, measure_sdb, sdb1)
        #thread_2 = threaderClass(2, measure_sdb, sdb2)

        #thread_2.start()
        thread_1.start()
        thread_1.join()
        #thread_2.join()
        print(datetime.datetime.now())
        print('done')
    exit()
